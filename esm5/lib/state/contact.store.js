import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromActions from './contact.actions';
import * as fromSelector from './contact.selectors';
var ContactStore = /** @class */ (function () {
    function ContactStore(store) {
        this.store = store;
    }
    Object.defineProperty(ContactStore.prototype, "Contacts$", {
        get: function () {
            return this.store.select(fromSelector.getContactItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Error$", {
        get: function () {
            return this.store.select(fromSelector.getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Success$", {
        get: function () {
            return this.store.select(fromSelector.getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Interactions$", {
        get: function () {
            return this.store.select(fromSelector.getContactInteractionItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "InteractionsError$", {
        get: function () {
            return this.store.select(fromSelector.getContactInteractionError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "InteractionsSuccess$", {
        get: function () {
            return this.store.select(fromSelector.getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.ContactById$ = function (id) {
        this.store.dispatch(fromActions.SelectContactAction({ contactId: id }));
        return this.store.select(fromSelector.getContactById);
    };
    Object.defineProperty(ContactStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(fromSelector.getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "LoadingInteractions$", {
        get: function () {
            return this.store.select(fromSelector.getIsLoadingInteractions);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.fetchContacts = function () {
        this.store.dispatch(fromActions.FetchContactBeginAction());
    };
    ContactStore.prototype.fetchInteractions$ = function (contactId) {
        this.store.dispatch(fromActions.FetchInteractionsBeginAction({ contactId: contactId }));
    };
    ContactStore.prototype.filterContacts = function (criteria) {
        this.store.dispatch(fromActions.FilterContactsBeginAction({ clientType: criteria }));
    };
    Object.defineProperty(ContactStore.prototype, "FilteredContacts$", {
        get: function () {
            return this.store.select(fromSelector.getFilteredContactItems);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.importContacts = function (contactList) {
        this.store.dispatch(fromActions.ImportContactBeginAction({ contactList: contactList }));
    };
    ContactStore.prototype.createContact = function (contactForm) {
        this.store.dispatch(fromActions.CreateContactBeginAction({ contactForm: contactForm }));
    };
    ContactStore.prototype.deleteContact = function (contactId) {
        this.store.dispatch(fromActions.DeleteContactBeginAction({ contactId: contactId }));
    };
    ContactStore.prototype.updateContact = function (contactForm) {
        this.store.dispatch(fromActions.UpdateContactBeginAction({ contactForm: contactForm }));
    };
    Object.defineProperty(ContactStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(fromSelector.hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.fetchCountryCodes = function () {
        this.store.dispatch(fromActions.FetchCountryCodesBeginAction());
    };
    Object.defineProperty(ContactStore.prototype, "LoadingCodes$", {
        get: function () {
            return this.store.select(fromSelector.getIsLoadingCountryCodes);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "LoadinCodesErrors$", {
        get: function () {
            return this.store.select(fromSelector.getCountryCodesErrors);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "CountryCodes$", {
        get: function () {
            return this.store.select(fromSelector.getCountryCodes);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.createInteraction = function (contactId, config) {
        this.store.dispatch(fromActions.CreateInteractionBeginAction({ contactId: contactId, config: config }));
    };
    ContactStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    ContactStore = __decorate([
        Injectable()
    ], ContactStore);
    return ContactStore;
}());
export { ContactStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC5zdG9yZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L2NvbnRhY3RzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc3RhdGUvY29udGFjdC5zdG9yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRXBDLE9BQU8sS0FBSyxXQUFXLE1BQU0sbUJBQW1CLENBQUM7QUFFakQsT0FBTyxLQUFLLFlBQVksTUFBTSxxQkFBcUIsQ0FBQztBQUdwRDtJQUNJLHNCQUFtQixLQUF1QztRQUF2QyxVQUFLLEdBQUwsS0FBSyxDQUFrQztJQUFJLENBQUM7SUFFL0Qsc0JBQUksbUNBQVM7YUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzNELENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0NBQU07YUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BELENBQUM7OztPQUFBO0lBRUQsc0JBQUksa0NBQVE7YUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELENBQUM7OztPQUFBO0lBRUQsc0JBQUksdUNBQWE7YUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1FBQ3RFLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNENBQWtCO2FBQXRCO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUN0RSxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDhDQUFvQjthQUF4QjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELENBQUM7OztPQUFBO0lBRUQsbUNBQVksR0FBWixVQUFhLEVBQVU7UUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4RSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsc0JBQUksa0NBQVE7YUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hELENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQW9CO2FBQXhCO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNwRSxDQUFDOzs7T0FBQTtJQUVELG9DQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCx5Q0FBa0IsR0FBbEIsVUFBbUIsU0FBaUI7UUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDRCQUE0QixDQUFDLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELHFDQUFjLEdBQWQsVUFBZSxRQUFzQjtRQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFDRCxzQkFBSSwyQ0FBaUI7YUFBckI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ25FLENBQUM7OztPQUFBO0lBRUQscUNBQWMsR0FBZCxVQUFlLFdBQStCO1FBQzFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFRCxvQ0FBYSxHQUFiLFVBQWMsV0FBeUI7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLEVBQUUsV0FBVyxhQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVELG9DQUFhLEdBQWIsVUFBYyxTQUFpQjtRQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsb0NBQWEsR0FBYixVQUFjLFdBQXlCO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFRCxzQkFBSSx5Q0FBZTthQUFuQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzFELENBQUM7OztPQUFBO0lBRUQsd0NBQWlCLEdBQWpCO1FBQ0ksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDRCQUE0QixFQUFFLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQsc0JBQUksdUNBQWE7YUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3BFLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNENBQWtCO2FBQXRCO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNqRSxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHVDQUFhO2FBQWpCO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDM0QsQ0FBQzs7O09BQUE7SUFFRCx3Q0FBaUIsR0FBakIsVUFBa0IsU0FBaUIsRUFBRSxNQUFnQztRQUNqRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsNEJBQTRCLENBQUMsRUFBRSxTQUFTLFdBQUEsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDOztnQkE1RnlCLEtBQUs7O0lBRHRCLFlBQVk7UUFEeEIsVUFBVSxFQUFFO09BQ0EsWUFBWSxDQThGeEI7SUFBRCxtQkFBQztDQUFBLEFBOUZELElBOEZDO1NBOUZZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdG9yZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IENPTlRBQ1RfVFlQRSwgSUNvbnRhY3RBcGlQcm9wcywgSUNvbnRhY3RGb3JtLCBJR2VuZXJpY0ludGVyYWN0aW9uUHJvcHMgfSBmcm9tICcuLi9yZXBvc2l0b3JpZXMvSUNvbnRhY3QucmVwb3NpdG9yeSc7XG5pbXBvcnQgKiBhcyBmcm9tQWN0aW9ucyBmcm9tICcuL2NvbnRhY3QuYWN0aW9ucyc7XG5pbXBvcnQgKiBhcyBmcm9tQ29udGFjdHMgZnJvbSAnLi9jb250YWN0LnJlZHVjZXInO1xuaW1wb3J0ICogYXMgZnJvbVNlbGVjdG9yIGZyb20gJy4vY29udGFjdC5zZWxlY3RvcnMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ29udGFjdFN0b3JlIHtcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgc3RvcmU6IFN0b3JlPGZyb21Db250YWN0cy5Db250YWN0U3RhdGU+KSB7IH1cblxuICAgIGdldCBDb250YWN0cyQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0Q29udGFjdEl0ZW1zKTtcbiAgICB9XG5cbiAgICBnZXQgRXJyb3IkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldEVycm9yKTtcbiAgICB9XG5cbiAgICBnZXQgU3VjY2VzcyQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0U3VjY2Vzcyk7XG4gICAgfVxuXG4gICAgZ2V0IEludGVyYWN0aW9ucyQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0Q29udGFjdEludGVyYWN0aW9uSXRlbXMpO1xuICAgIH1cblxuICAgIGdldCBJbnRlcmFjdGlvbnNFcnJvciQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0Q29udGFjdEludGVyYWN0aW9uRXJyb3IpO1xuICAgIH1cblxuICAgIGdldCBJbnRlcmFjdGlvbnNTdWNjZXNzJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRTdWNjZXNzKTtcbiAgICB9XG5cbiAgICBDb250YWN0QnlJZCQoaWQ6IG51bWJlcikge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLlNlbGVjdENvbnRhY3RBY3Rpb24oeyBjb250YWN0SWQ6IGlkIH0pKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRDb250YWN0QnlJZCk7XG4gICAgfVxuXG4gICAgZ2V0IExvYWRpbmckKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldElzTG9hZGluZyk7XG4gICAgfVxuXG4gICAgZ2V0IExvYWRpbmdJbnRlcmFjdGlvbnMkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldElzTG9hZGluZ0ludGVyYWN0aW9ucyk7XG4gICAgfVxuXG4gICAgZmV0Y2hDb250YWN0cygpIHtcbiAgICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQWN0aW9ucy5GZXRjaENvbnRhY3RCZWdpbkFjdGlvbigpKTtcbiAgICB9XG5cbiAgICBmZXRjaEludGVyYWN0aW9ucyQoY29udGFjdElkOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQWN0aW9ucy5GZXRjaEludGVyYWN0aW9uc0JlZ2luQWN0aW9uKHsgY29udGFjdElkIH0pKTtcbiAgICB9XG5cbiAgICBmaWx0ZXJDb250YWN0cyhjcml0ZXJpYTogQ09OVEFDVF9UWVBFKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRmlsdGVyQ29udGFjdHNCZWdpbkFjdGlvbih7IGNsaWVudFR5cGU6IGNyaXRlcmlhIH0pKTtcbiAgICB9XG4gICAgZ2V0IEZpbHRlcmVkQ29udGFjdHMkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldEZpbHRlcmVkQ29udGFjdEl0ZW1zKTtcbiAgICB9XG5cbiAgICBpbXBvcnRDb250YWN0cyhjb250YWN0TGlzdDogSUNvbnRhY3RBcGlQcm9wc1tdKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuSW1wb3J0Q29udGFjdEJlZ2luQWN0aW9uKHsgY29udGFjdExpc3QgfSkpO1xuICAgIH1cblxuICAgIGNyZWF0ZUNvbnRhY3QoY29udGFjdEZvcm06IElDb250YWN0Rm9ybSkge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkNyZWF0ZUNvbnRhY3RCZWdpbkFjdGlvbih7IGNvbnRhY3RGb3JtIH0pKTtcbiAgICB9XG5cbiAgICBkZWxldGVDb250YWN0KGNvbnRhY3RJZDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRGVsZXRlQ29udGFjdEJlZ2luQWN0aW9uKHsgY29udGFjdElkIH0pKTtcbiAgICB9XG5cbiAgICB1cGRhdGVDb250YWN0KGNvbnRhY3RGb3JtOiBJQ29udGFjdEZvcm0pIHtcbiAgICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQWN0aW9ucy5VcGRhdGVDb250YWN0QmVnaW5BY3Rpb24oeyBjb250YWN0Rm9ybSB9KSk7XG4gICAgfVxuXG4gICAgZ2V0IEhhc0JlZW5GZXRjaGVkJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5oYXNCZWVuRmV0Y2hlZCk7XG4gICAgfVxuXG4gICAgZmV0Y2hDb3VudHJ5Q29kZXMoKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRmV0Y2hDb3VudHJ5Q29kZXNCZWdpbkFjdGlvbigpKTtcbiAgICB9XG5cbiAgICBnZXQgTG9hZGluZ0NvZGVzJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRJc0xvYWRpbmdDb3VudHJ5Q29kZXMpO1xuICAgIH1cblxuICAgIGdldCBMb2FkaW5Db2Rlc0Vycm9ycyQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0Q291bnRyeUNvZGVzRXJyb3JzKTtcbiAgICB9XG5cbiAgICBnZXQgQ291bnRyeUNvZGVzJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRDb3VudHJ5Q29kZXMpO1xuICAgIH1cblxuICAgIGNyZWF0ZUludGVyYWN0aW9uKGNvbnRhY3RJZDogbnVtYmVyLCBjb25maWc6IElHZW5lcmljSW50ZXJhY3Rpb25Qcm9wcykge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkNyZWF0ZUludGVyYWN0aW9uQmVnaW5BY3Rpb24oeyBjb250YWN0SWQsIGNvbmZpZyB9KSk7XG4gICAgfVxufVxuIl19