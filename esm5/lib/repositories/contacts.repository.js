import { __decorate, __param } from "tslib";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { IHttpBasicResponse, APP_CONFIG_SERVICE, AbstractAppConfigService } from '@boxx/core';
var ContactsRepository = /** @class */ (function () {
    function ContactsRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    ContactsRepository.prototype.getContacts = function () {
        return this.httpClient.get("" + this.getBaseUrl());
    };
    ContactsRepository.prototype.getCountryCodes = function () {
        return this.httpClient.get(
        // `https://restcountries.eu/rest/v2/all?fields=flag;alpha3Code;callingCodes;translations;name`
        './../assets/countryCodes.json');
    };
    ContactsRepository.prototype.getContactInteractions = function (contactId) {
        return this.httpClient.get(this.getBaseUrl() + "/interactions/" + contactId);
    };
    ContactsRepository.prototype.createContact = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create", body);
    };
    ContactsRepository.prototype.deleteContact = function (contactId) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete/" + contactId);
    };
    ContactsRepository.prototype.updateContact = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update/" + payload.id, body);
    };
    ContactsRepository.prototype.importContacts = function (payload) {
        var data = { export_data: JSON.stringify({ contacts: payload }) };
        var urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach(function (key) {
            urlSearchParams.append(key, data[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/export_from_mobile", body);
    };
    ContactsRepository.prototype.createInteraction = function (contactId, config) {
        var urlSearchParams = new URLSearchParams();
        Object.keys(config).forEach(function (key) {
            urlSearchParams.append(key, config[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create_interaction/" + contactId, body);
    };
    ContactsRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/contacts";
    };
    ContactsRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    ContactsRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], ContactsRepository);
    return ContactsRepository;
}());
export { ContactsRepository };
/*export const countryCallingCodes:{[key: string]: string} = {
    //Country codes, source: https://countrycode.org

    //Note:
    //https://faq.whatsapp.com/sv/wp/21016748/?category=5245236
    //Make sure to remove any leading 0's or special calling codes
    //All phone numbers in Argentina (country code "54") should have a "9"
    // between the country code and area code. The prefix "15" must be removed so the
    // final number will have 13 digits total: +54 9 xxx xxx xxxx
    //Phone numbers in Mexico (country code "52") need to have "1" after "+52", even if they're Nextel numbers.

    'ARG': '54 9',//Argentina
    'MEX': '52 1',//México
  };*/
// Use the nex api: https://restcountries.eu/
// Example:
// https://restcountries.eu/rest/v2/{service}?fields={field};{field};{field}
// https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
// https://restcountries.eu/rest/v2/name/mexico?fields=flag;alpha3Code;callingCodes;translations
export var COUNTRY_CALLING_CODES = [
    { alpha3Code: 'ARG', callingCodes: ['54'], flag: 'https://restcountries.eu/data/arg.svg', name: 'Argentina', translations: { es: 'Argentina' } },
    {
        alpha3Code: 'MEX',
        callingCodes: ['52'],
        flag: 'https://restcountries.eu/data/mex.svg',
        name: 'México', translations: { es: 'México' }
    },
    { alpha3Code: 'USA', callingCodes: ['1'], flag: 'https://restcountries.eu/data/usa.svg', name: 'Estado Unidos', translations: { es: 'Estado Unidos' } },
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdHMucmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L2NvbnRhY3RzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvcmVwb3NpdG9yaWVzL2NvbnRhY3RzLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFXbkQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLHdCQUF3QixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBRzlGO0lBRUksNEJBQ3dDLFdBQXFDLEVBQ2pFLFVBQXNCO1FBRE0sZ0JBQVcsR0FBWCxXQUFXLENBQTBCO1FBQ2pFLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDOUIsQ0FBQztJQUVMLHdDQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUE4QyxLQUFHLElBQUksQ0FBQyxVQUFVLEVBQUksQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFFRCw0Q0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUc7UUFDdEIsK0ZBQStGO1FBQy9GLCtCQUErQixDQUNsQyxDQUFDO0lBQ04sQ0FBQztJQUVELG1EQUFzQixHQUF0QixVQUF1QixTQUFpQjtRQUNwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsVUFBVSxFQUFFLHNCQUFpQixTQUFXLENBQ25ELENBQUM7SUFDTixDQUFDO0lBRUQsMENBQWEsR0FBYixVQUFjLE9BQXFCO1FBQy9CLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFFOUIsS0FBSyxJQUFNLEdBQUcsSUFBSSxPQUFPLEVBQUU7WUFDdkIsSUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUM3QixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDN0M7U0FDSjtRQUVELElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUUvQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUEwQyxJQUFJLENBQUMsVUFBVSxFQUFFLFlBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRyxDQUFDO0lBRUQsMENBQWEsR0FBYixVQUFjLFNBQWlCO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQThCLElBQUksQ0FBQyxVQUFVLEVBQUUsZ0JBQVcsU0FBVyxDQUFDLENBQUM7SUFDeEcsQ0FBQztJQUVELDBDQUFhLEdBQWIsVUFBYyxPQUFxQjtRQUMvQixJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBRTlCLEtBQUssSUFBTSxHQUFHLElBQUksT0FBTyxFQUFFO1lBQ3ZCLElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDN0IsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQzdDO1NBQ0o7UUFFRCxJQUFNLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBMEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxnQkFBVyxPQUFPLENBQUMsRUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pILENBQUM7SUFFRCwyQ0FBYyxHQUFkLFVBQWUsT0FBMkI7UUFFdEMsSUFBTSxJQUFJLEdBQUcsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFFcEUsSUFBTSxlQUFlLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQztRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQVc7WUFDbEMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFNLElBQUksR0FBRyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFeEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDcEIsSUFBSSxDQUFDLFVBQVUsRUFBRSx3QkFBcUIsRUFDekMsSUFBSSxDQUNQLENBQUM7SUFDTixDQUFDO0lBRUQsOENBQWlCLEdBQWpCLFVBQWtCLFNBQWlCLEVBQUUsTUFBZ0M7UUFFakUsSUFBTSxlQUFlLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQztRQUM5QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQVc7WUFDcEMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFNLElBQUksR0FBRyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFeEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDcEIsSUFBSSxDQUFDLFVBQVUsRUFBRSw0QkFBdUIsU0FBVyxFQUFFLElBQUksQ0FDL0QsQ0FBQztJQUNOLENBQUM7SUFFTyx1Q0FBVSxHQUFsQjtRQUNJLE9BQVUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsYUFBUSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxpQkFBYyxDQUFDO0lBQzFGLENBQUM7O2dCQXJGb0Qsd0JBQXdCLHVCQUF4RSxNQUFNLFNBQUMsa0JBQWtCO2dCQUNOLFVBQVU7O0lBSnpCLGtCQUFrQjtRQUQ5QixVQUFVLEVBQUU7UUFJSixXQUFBLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO09BSHRCLGtCQUFrQixDQXlGOUI7SUFBRCx5QkFBQztDQUFBLEFBekZELElBeUZDO1NBekZZLGtCQUFrQjtBQThGL0I7Ozs7Ozs7Ozs7Ozs7TUFhTTtBQUVOLDZDQUE2QztBQUM3QyxXQUFXO0FBQ1gsNEVBQTRFO0FBQzVFLHNFQUFzRTtBQUN0RSxnR0FBZ0c7QUFDaEcsTUFBTSxDQUFDLElBQU0scUJBQXFCLEdBQUc7SUFDakMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSx1Q0FBdUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsRUFBRTtJQUNoSjtRQUNJLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQztRQUNwQixJQUFJLEVBQUUsdUNBQXVDO1FBQzdDLElBQUksRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRTtLQUNqRDtJQUNELEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsdUNBQXVDLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLEVBQUUsZUFBZSxFQUFFLEVBQUU7Q0FDMUosQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7XG4gICAgSUNvbnRhY3RBcGlQcm9wcyxcbiAgICBJQ29udGFjdEZvcm0sXG4gICAgSUNvbnRhY3RJbnRlcmFjdGlvbnNBcGlQcm9wcyxcbiAgICBJQ29udGFjdFJlcG9zaXRvcnksXG4gICAgSUNvdW50cnlDb2RlcyxcbiAgICBJR2VuZXJpY0ludGVyYWN0aW9uUHJvcHMsXG4gICAgSUltcG9ydENvbnRhY3RzUmVzcG9uc2Vcbn0gZnJvbSAnLi9JQ29udGFjdC5yZXBvc2l0b3J5JztcbmltcG9ydCB7IElIdHRwQmFzaWNSZXNwb25zZSwgQVBQX0NPTkZJR19TRVJWSUNFLCBBYnN0cmFjdEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICdAYm94eC9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIENvbnRhY3RzUmVwb3NpdG9yeSBpbXBsZW1lbnRzIElDb250YWN0UmVwb3NpdG9yeSB7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChBUFBfQ09ORklHX1NFUlZJQ0UpIHByaXZhdGUgYXBwU2V0dGluZ3M6IEFic3RyYWN0QXBwQ29uZmlnU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxuICAgICkgeyB9XG5cbiAgICBnZXRDb250YWN0cygpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxBcnJheTxJQ29udGFjdEFwaVByb3BzPj4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPEFycmF5PElDb250YWN0QXBpUHJvcHM+Pj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9YCk7XG4gICAgfVxuXG4gICAgZ2V0Q291bnRyeUNvZGVzKCk6IE9ic2VydmFibGU8QXJyYXk8SUNvdW50cnlDb2Rlcz4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8QXJyYXk8SUNvdW50cnlDb2Rlcz4+KFxuICAgICAgICAgICAgLy8gYGh0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9yZXN0L3YyL2FsbD9maWVsZHM9ZmxhZzthbHBoYTNDb2RlO2NhbGxpbmdDb2Rlczt0cmFuc2xhdGlvbnM7bmFtZWBcbiAgICAgICAgICAgICcuLy4uL2Fzc2V0cy9jb3VudHJ5Q29kZXMuanNvbidcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXRDb250YWN0SW50ZXJhY3Rpb25zKGNvbnRhY3RJZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8QXJyYXk8SUNvbnRhY3RJbnRlcmFjdGlvbnNBcGlQcm9wcz4+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PElIdHRwQmFzaWNSZXNwb25zZTxBcnJheTxJQ29udGFjdEludGVyYWN0aW9uc0FwaVByb3BzPj4+KFxuICAgICAgICAgICAgYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2ludGVyYWN0aW9ucy8ke2NvbnRhY3RJZH1gXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgY3JlYXRlQ29udGFjdChwYXlsb2FkOiBJQ29udGFjdEZvcm0pOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJQ29udGFjdEFwaVByb3BzPj4ge1xuICAgICAgICBsZXQgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKTtcblxuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBwYXlsb2FkKSB7XG4gICAgICAgICAgICBpZiAocGF5bG9hZC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIHBheWxvYWRba2V5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBib2R5ID0gcGFyYW1zLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PElIdHRwQmFzaWNSZXNwb25zZTxJQ29udGFjdEFwaVByb3BzPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZWAsIGJvZHkpO1xuICAgIH1cblxuICAgIGRlbGV0ZUNvbnRhY3QoY29udGFjdElkOiBudW1iZXIpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxudWxsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmRlbGV0ZTxJSHR0cEJhc2ljUmVzcG9uc2U8bnVsbD4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9kZWxldGUvJHtjb250YWN0SWR9YCk7XG4gICAgfVxuXG4gICAgdXBkYXRlQ29udGFjdChwYXlsb2FkOiBJQ29udGFjdEZvcm0pOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJQ29udGFjdEFwaVByb3BzPj4ge1xuICAgICAgICBsZXQgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKTtcblxuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBwYXlsb2FkKSB7XG4gICAgICAgICAgICBpZiAocGF5bG9hZC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIHBheWxvYWRba2V5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBib2R5ID0gcGFyYW1zLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PElIdHRwQmFzaWNSZXNwb25zZTxJQ29udGFjdEFwaVByb3BzPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3VwZGF0ZS8ke3BheWxvYWQuaWR9YCwgYm9keSk7XG4gICAgfVxuXG4gICAgaW1wb3J0Q29udGFjdHMocGF5bG9hZDogSUNvbnRhY3RBcGlQcm9wc1tdKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SUltcG9ydENvbnRhY3RzUmVzcG9uc2U+PiB7XG5cbiAgICAgICAgY29uc3QgZGF0YSA9IHsgZXhwb3J0X2RhdGE6IEpTT04uc3RyaW5naWZ5KHsgY29udGFjdHM6IHBheWxvYWQgfSkgfTtcblxuICAgICAgICBjb25zdCB1cmxTZWFyY2hQYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKCk7XG4gICAgICAgIE9iamVjdC5rZXlzKGRhdGEpLmZvckVhY2goKGtleTogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICB1cmxTZWFyY2hQYXJhbXMuYXBwZW5kKGtleSwgZGF0YVtrZXldKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3QgYm9keSA9IHVybFNlYXJjaFBhcmFtcy50b1N0cmluZygpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8SUltcG9ydENvbnRhY3RzUmVzcG9uc2U+PihcbiAgICAgICAgICAgIGAke3RoaXMuZ2V0QmFzZVVybCgpfS9leHBvcnRfZnJvbV9tb2JpbGVgLFxuICAgICAgICAgICAgYm9keVxuICAgICAgICApO1xuICAgIH1cblxuICAgIGNyZWF0ZUludGVyYWN0aW9uKGNvbnRhY3RJZDogbnVtYmVyLCBjb25maWc6IElHZW5lcmljSW50ZXJhY3Rpb25Qcm9wcyk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElDb250YWN0SW50ZXJhY3Rpb25zQXBpUHJvcHM+PiB7XG5cbiAgICAgICAgY29uc3QgdXJsU2VhcmNoUGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICBPYmplY3Qua2V5cyhjb25maWcpLmZvckVhY2goKGtleTogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICB1cmxTZWFyY2hQYXJhbXMuYXBwZW5kKGtleSwgY29uZmlnW2tleV0pO1xuICAgICAgICB9KTtcbiAgICAgICAgY29uc3QgYm9keSA9IHVybFNlYXJjaFBhcmFtcy50b1N0cmluZygpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8SUNvbnRhY3RJbnRlcmFjdGlvbnNBcGlQcm9wcz4+KFxuICAgICAgICAgICAgYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZV9pbnRlcmFjdGlvbi8ke2NvbnRhY3RJZH1gLCBib2R5XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRCYXNlVXJsKCnCoHtcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuYXBwU2V0dGluZ3MuYmFzZVVybCgpfS9hcGkvJHt0aGlzLmFwcFNldHRpbmdzLmluc3RhbmNlKCl9L3YxL2NvbnRhY3RzYDtcbiAgICB9XG59XG5cblxuXG5cbi8qZXhwb3J0IGNvbnN0IGNvdW50cnlDYWxsaW5nQ29kZXM6e1trZXk6IHN0cmluZ106IHN0cmluZ30gPSB7XG4gICAgLy9Db3VudHJ5IGNvZGVzLCBzb3VyY2U6IGh0dHBzOi8vY291bnRyeWNvZGUub3JnXG5cbiAgICAvL05vdGU6XG4gICAgLy9odHRwczovL2ZhcS53aGF0c2FwcC5jb20vc3Yvd3AvMjEwMTY3NDgvP2NhdGVnb3J5PTUyNDUyMzZcbiAgICAvL01ha2Ugc3VyZSB0byByZW1vdmUgYW55IGxlYWRpbmcgMCdzIG9yIHNwZWNpYWwgY2FsbGluZyBjb2Rlc1xuICAgIC8vQWxsIHBob25lIG51bWJlcnMgaW4gQXJnZW50aW5hIChjb3VudHJ5IGNvZGUgXCI1NFwiKSBzaG91bGQgaGF2ZSBhIFwiOVwiXG4gICAgLy8gYmV0d2VlbiB0aGUgY291bnRyeSBjb2RlIGFuZCBhcmVhIGNvZGUuIFRoZSBwcmVmaXggXCIxNVwiIG11c3QgYmUgcmVtb3ZlZCBzbyB0aGVcbiAgICAvLyBmaW5hbCBudW1iZXIgd2lsbCBoYXZlIDEzIGRpZ2l0cyB0b3RhbDogKzU0IDkgeHh4IHh4eCB4eHh4XG4gICAgLy9QaG9uZSBudW1iZXJzIGluIE1leGljbyAoY291bnRyeSBjb2RlIFwiNTJcIikgbmVlZCB0byBoYXZlIFwiMVwiIGFmdGVyIFwiKzUyXCIsIGV2ZW4gaWYgdGhleSdyZSBOZXh0ZWwgbnVtYmVycy5cblxuICAgICdBUkcnOiAnNTQgOScsLy9BcmdlbnRpbmFcbiAgICAnTUVYJzogJzUyIDEnLC8vTcOpeGljb1xuICB9OyovXG5cbi8vIFVzZSB0aGUgbmV4IGFwaTogaHR0cHM6Ly9yZXN0Y291bnRyaWVzLmV1L1xuLy8gRXhhbXBsZTpcbi8vIGh0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9yZXN0L3YyL3tzZXJ2aWNlfT9maWVsZHM9e2ZpZWxkfTt7ZmllbGR9O3tmaWVsZH1cbi8vIGh0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9yZXN0L3YyL2FsbD9maWVsZHM9bmFtZTtjYXBpdGFsO2N1cnJlbmNpZXNcbi8vIGh0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9yZXN0L3YyL25hbWUvbWV4aWNvP2ZpZWxkcz1mbGFnO2FscGhhM0NvZGU7Y2FsbGluZ0NvZGVzO3RyYW5zbGF0aW9uc1xuZXhwb3J0IGNvbnN0IENPVU5UUllfQ0FMTElOR19DT0RFUyA9IFtcbiAgICB7IGFscGhhM0NvZGU6ICdBUkcnLCBjYWxsaW5nQ29kZXM6IFsnNTQnXSwgZmxhZzogJ2h0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9kYXRhL2FyZy5zdmcnLCBuYW1lOiAnQXJnZW50aW5hJywgdHJhbnNsYXRpb25zOiB7IGVzOiAnQXJnZW50aW5hJyB9IH0sXG4gICAge1xuICAgICAgICBhbHBoYTNDb2RlOiAnTUVYJyxcbiAgICAgICAgY2FsbGluZ0NvZGVzOiBbJzUyJ10sXG4gICAgICAgIGZsYWc6ICdodHRwczovL3Jlc3Rjb3VudHJpZXMuZXUvZGF0YS9tZXguc3ZnJyxcbiAgICAgICAgbmFtZTogJ03DqXhpY28nLCB0cmFuc2xhdGlvbnM6IHsgZXM6ICdNw6l4aWNvJyB9XG4gICAgfSxcbiAgICB7IGFscGhhM0NvZGU6ICdVU0EnLCBjYWxsaW5nQ29kZXM6IFsnMSddLCBmbGFnOiAnaHR0cHM6Ly9yZXN0Y291bnRyaWVzLmV1L2RhdGEvdXNhLnN2ZycsIG5hbWU6ICdFc3RhZG8gVW5pZG9zJywgdHJhbnNsYXRpb25zOiB7IGVzOiAnRXN0YWRvIFVuaWRvcycgfSB9LFxuXTtcbiJdfQ==