import { __decorate, __read, __spread } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { ContactsRepository } from './repositories/contacts.repository';
import { CONTACTS_REPOSITORY } from './repositories/IContact.repository';
import { ContactsService } from './services/contacts.service';
import { CONTACTS_SERVICE } from './services/IContact.service';
import { ContactsEffects } from './state/contact.effects';
import { contactsReducer } from './state/contact.reducer';
import { ContactStore } from './state/contact.store';
var ContactsCoreModule = /** @class */ (function () {
    function ContactsCoreModule() {
    }
    ContactsCoreModule_1 = ContactsCoreModule;
    ContactsCoreModule.forRoot = function (config) {
        return {
            ngModule: ContactsCoreModule_1,
            providers: __spread([
                { provide: CONTACTS_SERVICE, useClass: ContactsService },
                { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository }
            ], config.providers, [
                Contacts,
                ContactStore
            ])
        };
    };
    var ContactsCoreModule_1;
    ContactsCoreModule = ContactsCoreModule_1 = __decorate([
        NgModule({
            declarations: [
                ContactItemComponent
            ],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('contacts', contactsReducer),
                EffectsModule.forFeature([ContactsEffects]),
                CommonModule,
                IonicModule
            ],
            exports: [
                ContactItemComponent
            ]
        })
    ], ContactsCoreModule);
    return ContactsCoreModule;
}());
export { ContactsCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdHMtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9jb250YWN0cy1jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbnRhY3RzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDekUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBcUJyRDtJQUFBO0lBY0EsQ0FBQzsyQkFkWSxrQkFBa0I7SUFDcEIsMEJBQU8sR0FBZCxVQUFlLE1BQThCO1FBRXpDLE9BQU87WUFDSCxRQUFRLEVBQUUsb0JBQWtCO1lBQzVCLFNBQVM7Z0JBQ0wsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTtnQkFDeEQsRUFBRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFO2VBQzNELE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixRQUFRO2dCQUNSLFlBQVk7Y0FDZjtTQUNKLENBQUM7SUFDTixDQUFDOztJQWJRLGtCQUFrQjtRQWY5QixRQUFRLENBQUM7WUFDUixZQUFZLEVBQUU7Z0JBQ1osb0JBQW9CO2FBQ3JCO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLGdCQUFnQjtnQkFDaEIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsZUFBZSxDQUFDO2dCQUNuRCxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzNDLFlBQVk7Z0JBQ1osV0FBVzthQUNaO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLG9CQUFvQjthQUNyQjtTQUNGLENBQUM7T0FDVyxrQkFBa0IsQ0FjOUI7SUFBRCx5QkFBQztDQUFBLEFBZEQsSUFjQztTQWRZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb250YWN0cyB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29udGFjdHMnO1xuaW1wb3J0IHsgSW9uaWNNb2R1bGUgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBFZmZlY3RzTW9kdWxlIH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IENvbnRhY3RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbnRhY3QtaXRlbS9jb250YWN0LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRhY3RzUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL2NvbnRhY3RzLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgQ09OVEFDVFNfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lDb250YWN0LnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgQ29udGFjdHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb250YWN0cy5zZXJ2aWNlJztcbmltcG9ydCB7IENPTlRBQ1RTX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lDb250YWN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29udGFjdHNFZmZlY3RzIH0gZnJvbSAnLi9zdGF0ZS9jb250YWN0LmVmZmVjdHMnO1xuaW1wb3J0IHsgY29udGFjdHNSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS9jb250YWN0LnJlZHVjZXInO1xuaW1wb3J0IHsgQ29udGFjdFN0b3JlIH0gZnJvbSAnLi9zdGF0ZS9jb250YWN0LnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQ29udGFjdEl0ZW1Db21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgU3RvcmVNb2R1bGUuZm9yRmVhdHVyZSgnY29udGFjdHMnLCBjb250YWN0c1JlZHVjZXIpLFxuICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbQ29udGFjdHNFZmZlY3RzXSksXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIElvbmljTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBDb250YWN0SXRlbUNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RzQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBNb2R1bGVPcHRpb25zSW50ZXJmYWNlKTogTW9kdWxlV2l0aFByb3ZpZGVyczxDb250YWN0c0NvcmVNb2R1bGU+IHtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IENvbnRhY3RzQ29yZU1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogQ09OVEFDVFNfU0VSVklDRSwgdXNlQ2xhc3M6IENvbnRhY3RzU2VydmljZSB9LFxuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogQ09OVEFDVFNfUkVQT1NJVE9SWSwgdXNlQ2xhc3M6IENvbnRhY3RzUmVwb3NpdG9yeSB9LFxuICAgICAgICAgICAgICAgIC4uLmNvbmZpZy5wcm92aWRlcnMsXG4gICAgICAgICAgICAgICAgQ29udGFjdHMsXG4gICAgICAgICAgICAgICAgQ29udGFjdFN0b3JlXG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgfVxufVxuIl19