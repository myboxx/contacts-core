import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let ContactItemComponent = class ContactItemComponent {
    constructor() {
        this.showDetail = true;
        this.showEmail = false;
    }
    ngOnInit() { }
};
__decorate([
    Input()
], ContactItemComponent.prototype, "showDetail", void 0);
__decorate([
    Input()
], ContactItemComponent.prototype, "contact", void 0);
__decorate([
    Input()
], ContactItemComponent.prototype, "showEmail", void 0);
ContactItemComponent = __decorate([
    Component({
        selector: 'boxx-contact-item',
        template: "<ion-item detail=\"{{showDetail}}\">\n    <ion-avatar slot=\"start\" class=\"boxx-contact-item-avatar\">\n        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n    </ion-avatar>\n    <ion-label>\n        <h2>{{contact.name}} {{contact.lastName}}</h2>\n        <p class=\"boxx-contact-item-detail email\" *ngIf=\"contact.email && showEmail\">\n            <ion-icon class=\"boxx-contact-item-detail-icon email\" name=\"mail-open\"></ion-icon>\n            {{contact.email}}\n        </p>\n        <p class=\"boxx-contact-item-detail phone\" *ngIf=\"contact.phone\">\n            <ion-icon class=\"boxx-contact-item-detail-icon phone\" name=\"call\"></ion-icon>\n            {{contact.phone}}\n        </p>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
        styles: [".contact-icon-detail{display:flex;align-items:center}.contact-icon-detail .boxx-contact-item-detail-icon{margin-right:8px}"]
    })
], ContactItemComponent);
export { ContactItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L2NvbnRhY3RzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb250YWN0LWl0ZW0vY29udGFjdC1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFRekQsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFLL0I7UUFKVyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBRWxCLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFFYixDQUFDO0lBRWpCLFFBQVEsS0FBSSxDQUFDO0NBRWQsQ0FBQTtBQVJZO0lBQVIsS0FBSyxFQUFFO3dEQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTtxREFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7dURBQW1CO0FBSGxCLG9CQUFvQjtJQUxoQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLHl5QkFBNEM7O0tBRTdDLENBQUM7R0FDVyxvQkFBb0IsQ0FTaEM7U0FUWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbnRhY3RNb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jb250YWN0Lm1vZGVsJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC1jb250YWN0LWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogJy4vY29udGFjdC1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY29udGFjdC1pdGVtLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBzaG93RGV0YWlsID0gdHJ1ZTtcbiAgICBASW5wdXQoKSBjb250YWN0OiBDb250YWN0TW9kZWw7XG4gICAgQElucHV0KCkgc2hvd0VtYWlsID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHt9XG5cbn1cbiJdfQ==