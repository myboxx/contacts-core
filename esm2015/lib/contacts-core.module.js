var ContactsCoreModule_1;
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { ContactsRepository } from './repositories/contacts.repository';
import { CONTACTS_REPOSITORY } from './repositories/IContact.repository';
import { ContactsService } from './services/contacts.service';
import { CONTACTS_SERVICE } from './services/IContact.service';
import { ContactsEffects } from './state/contact.effects';
import { contactsReducer } from './state/contact.reducer';
import { ContactStore } from './state/contact.store';
let ContactsCoreModule = ContactsCoreModule_1 = class ContactsCoreModule {
    static forRoot(config) {
        return {
            ngModule: ContactsCoreModule_1,
            providers: [
                { provide: CONTACTS_SERVICE, useClass: ContactsService },
                { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository },
                ...config.providers,
                Contacts,
                ContactStore
            ]
        };
    }
};
ContactsCoreModule = ContactsCoreModule_1 = __decorate([
    NgModule({
        declarations: [
            ContactItemComponent
        ],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('contacts', contactsReducer),
            EffectsModule.forFeature([ContactsEffects]),
            CommonModule,
            IonicModule
        ],
        exports: [
            ContactItemComponent
        ]
    })
], ContactsCoreModule);
export { ContactsCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdHMtY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9jb250YWN0cy1jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbnRhY3RzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzFELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQXFCckQsSUFBYSxrQkFBa0IsMEJBQS9CLE1BQWEsa0JBQWtCO0lBQzNCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBOEI7UUFFekMsT0FBTztZQUNILFFBQVEsRUFBRSxvQkFBa0I7WUFDNUIsU0FBUyxFQUFFO2dCQUNQLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7Z0JBQ3hELEVBQUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRTtnQkFDOUQsR0FBRyxNQUFNLENBQUMsU0FBUztnQkFDbkIsUUFBUTtnQkFDUixZQUFZO2FBQ2Y7U0FDSixDQUFDO0lBQ04sQ0FBQztDQUNKLENBQUE7QUFkWSxrQkFBa0I7SUFmOUIsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osb0JBQW9CO1NBQ3JCO1FBQ0QsT0FBTyxFQUFFO1lBQ1AsZ0JBQWdCO1lBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLGVBQWUsQ0FBQztZQUNuRCxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0MsWUFBWTtZQUNaLFdBQVc7U0FDWjtRQUNELE9BQU8sRUFBRTtZQUNQLG9CQUFvQjtTQUNyQjtLQUNGLENBQUM7R0FDVyxrQkFBa0IsQ0FjOUI7U0FkWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29udGFjdHMgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvbnRhY3RzJztcbmltcG9ydCB7IElvbmljTW9kdWxlIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgRWZmZWN0c01vZHVsZSB9IGZyb20gJ0BuZ3J4L2VmZmVjdHMnO1xuaW1wb3J0IHsgU3RvcmVNb2R1bGUgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQgeyBDb250YWN0SXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250YWN0LWl0ZW0vY29udGFjdC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250YWN0c1JlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9jb250YWN0cy5yZXBvc2l0b3J5JztcbmltcG9ydCB7IENPTlRBQ1RTX1JFUE9TSVRPUlkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9JQ29udGFjdC5yZXBvc2l0b3J5JztcbmltcG9ydCB7IENvbnRhY3RzU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvY29udGFjdHMuc2VydmljZSc7XG5pbXBvcnQgeyBDT05UQUNUU19TRVJWSUNFIH0gZnJvbSAnLi9zZXJ2aWNlcy9JQ29udGFjdC5zZXJ2aWNlJztcbmltcG9ydCB7IENvbnRhY3RzRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvY29udGFjdC5lZmZlY3RzJztcbmltcG9ydCB7IGNvbnRhY3RzUmVkdWNlciB9IGZyb20gJy4vc3RhdGUvY29udGFjdC5yZWR1Y2VyJztcbmltcG9ydCB7IENvbnRhY3RTdG9yZSB9IGZyb20gJy4vc3RhdGUvY29udGFjdC5zdG9yZSc7XG5cbmludGVyZmFjZSBNb2R1bGVPcHRpb25zSW50ZXJmYWNlIHtcbiAgICBwcm92aWRlcnM6IFByb3ZpZGVyW107XG59XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIENvbnRhY3RJdGVtQ29tcG9uZW50XG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIFN0b3JlTW9kdWxlLmZvckZlYXR1cmUoJ2NvbnRhY3RzJywgY29udGFjdHNSZWR1Y2VyKSxcbiAgICBFZmZlY3RzTW9kdWxlLmZvckZlYXR1cmUoW0NvbnRhY3RzRWZmZWN0c10pLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBJb25pY01vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgQ29udGFjdEl0ZW1Db21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBDb250YWN0c0NvcmVNb2R1bGUge1xuICAgIHN0YXRpYyBmb3JSb290KGNvbmZpZzogTW9kdWxlT3B0aW9uc0ludGVyZmFjZSk6IE1vZHVsZVdpdGhQcm92aWRlcnM8Q29udGFjdHNDb3JlTW9kdWxlPiB7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG5nTW9kdWxlOiBDb250YWN0c0NvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IENPTlRBQ1RTX1NFUlZJQ0UsIHVzZUNsYXNzOiBDb250YWN0c1NlcnZpY2UgfSxcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IENPTlRBQ1RTX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBDb250YWN0c1JlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIENvbnRhY3RzLFxuICAgICAgICAgICAgICAgIENvbnRhY3RTdG9yZVxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgIH1cbn1cbiJdfQ==