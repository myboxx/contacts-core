(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/common/http'), require('@angular/core'), require('@ionic-native/contacts'), require('@ionic/angular'), require('@ngrx/effects'), require('@ngrx/store'), require('@boxx/core'), require('rxjs'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('@boxx/contacts-core', ['exports', '@angular/common', '@angular/common/http', '@angular/core', '@ionic-native/contacts', '@ionic/angular', '@ngrx/effects', '@ngrx/store', '@boxx/core', 'rxjs', 'rxjs/operators'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['contacts-core'] = {}), global.ng.common, global.ng.common.http, global.ng.core, global['ionic-native-contacts'], global['ionic-angular'], global['ngrx-effects'], global['ngrx-store'], global['boxx-core'], global.rxjs, global.rxjs.operators));
}(this, (function (exports, common, http, core, contacts, angular, effects, store, core$1, rxjs, operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var ContactItemComponent = /** @class */ (function () {
        function ContactItemComponent() {
            this.showDetail = true;
            this.showEmail = false;
        }
        ContactItemComponent.prototype.ngOnInit = function () { };
        __decorate([
            core.Input()
        ], ContactItemComponent.prototype, "showDetail", void 0);
        __decorate([
            core.Input()
        ], ContactItemComponent.prototype, "contact", void 0);
        __decorate([
            core.Input()
        ], ContactItemComponent.prototype, "showEmail", void 0);
        ContactItemComponent = __decorate([
            core.Component({
                selector: 'boxx-contact-item',
                template: "<ion-item detail=\"{{showDetail}}\">\n    <ion-avatar slot=\"start\" class=\"boxx-contact-item-avatar\">\n        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n    </ion-avatar>\n    <ion-label>\n        <h2>{{contact.name}} {{contact.lastName}}</h2>\n        <p class=\"boxx-contact-item-detail email\" *ngIf=\"contact.email && showEmail\">\n            <ion-icon class=\"boxx-contact-item-detail-icon email\" name=\"mail-open\"></ion-icon>\n            {{contact.email}}\n        </p>\n        <p class=\"boxx-contact-item-detail phone\" *ngIf=\"contact.phone\">\n            <ion-icon class=\"boxx-contact-item-detail-icon phone\" name=\"call\"></ion-icon>\n            {{contact.phone}}\n        </p>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
                styles: [".contact-icon-detail{display:flex;align-items:center}.contact-icon-detail .boxx-contact-item-detail-icon{margin-right:8px}"]
            })
        ], ContactItemComponent);
        return ContactItemComponent;
    }());

    var ContactsRepository = /** @class */ (function () {
        function ContactsRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        ContactsRepository.prototype.getContacts = function () {
            return this.httpClient.get("" + this.getBaseUrl());
        };
        ContactsRepository.prototype.getCountryCodes = function () {
            return this.httpClient.get(
            // `https://restcountries.eu/rest/v2/all?fields=flag;alpha3Code;callingCodes;translations;name`
            './../assets/countryCodes.json');
        };
        ContactsRepository.prototype.getContactInteractions = function (contactId) {
            return this.httpClient.get(this.getBaseUrl() + "/interactions/" + contactId);
        };
        ContactsRepository.prototype.createContact = function (payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/create", body);
        };
        ContactsRepository.prototype.deleteContact = function (contactId) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete/" + contactId);
        };
        ContactsRepository.prototype.updateContact = function (payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/update/" + payload.id, body);
        };
        ContactsRepository.prototype.importContacts = function (payload) {
            var data = { export_data: JSON.stringify({ contacts: payload }) };
            var urlSearchParams = new URLSearchParams();
            Object.keys(data).forEach(function (key) {
                urlSearchParams.append(key, data[key]);
            });
            var body = urlSearchParams.toString();
            return this.httpClient.post(this.getBaseUrl() + "/export_from_mobile", body);
        };
        ContactsRepository.prototype.createInteraction = function (contactId, config) {
            var urlSearchParams = new URLSearchParams();
            Object.keys(config).forEach(function (key) {
                urlSearchParams.append(key, config[key]);
            });
            var body = urlSearchParams.toString();
            return this.httpClient.post(this.getBaseUrl() + "/create_interaction/" + contactId, body);
        };
        ContactsRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/contacts";
        };
        ContactsRepository.ctorParameters = function () { return [
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        ContactsRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], ContactsRepository);
        return ContactsRepository;
    }());
    /*export const countryCallingCodes:{[key: string]: string} = {
        //Country codes, source: https://countrycode.org

        //Note:
        //https://faq.whatsapp.com/sv/wp/21016748/?category=5245236
        //Make sure to remove any leading 0's or special calling codes
        //All phone numbers in Argentina (country code "54") should have a "9"
        // between the country code and area code. The prefix "15" must be removed so the
        // final number will have 13 digits total: +54 9 xxx xxx xxxx
        //Phone numbers in Mexico (country code "52") need to have "1" after "+52", even if they're Nextel numbers.

        'ARG': '54 9',//Argentina
        'MEX': '52 1',//México
      };*/
    // Use the nex api: https://restcountries.eu/
    // Example:
    // https://restcountries.eu/rest/v2/{service}?fields={field};{field};{field}
    // https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
    // https://restcountries.eu/rest/v2/name/mexico?fields=flag;alpha3Code;callingCodes;translations
    var COUNTRY_CALLING_CODES = [
        { alpha3Code: 'ARG', callingCodes: ['54'], flag: 'https://restcountries.eu/data/arg.svg', name: 'Argentina', translations: { es: 'Argentina' } },
        {
            alpha3Code: 'MEX',
            callingCodes: ['52'],
            flag: 'https://restcountries.eu/data/mex.svg',
            name: 'México', translations: { es: 'México' }
        },
        { alpha3Code: 'USA', callingCodes: ['1'], flag: 'https://restcountries.eu/data/usa.svg', name: 'Estado Unidos', translations: { es: 'Estado Unidos' } },
    ];

    var CONTACTS_REPOSITORY = new core.InjectionToken('contactsRepository');

    var ContactModel = /** @class */ (function () {
        function ContactModel(data) {
            this.id = data.id;
            this.name = data.name;
            this.lastName = data.lastName || '';
            this.type = data.type || 'NOT_SPECIFIED';
            this.origin = data.origin || 'UNKNOWN';
            this.email = data.email || '';
            this.phone = (data.phone || '').replace(/[^0-9]+/g, '').slice(-10);
            this.countryCode = data.countryCode || 'MEX';
            this.phoneCode = data.phoneCode || '+52';
            this.streetAddress = data.streetAddress || '';
            this.city = data.city || '';
            this.stateIso = data.stateIso || '';
            this.createdAt = data.createdAt || '';
            this.updatedAt = data.updatedAt || '';
        }
        ContactModel.toApiModel = function (contact, excludedFields) {
            if (excludedFields === void 0) { excludedFields = []; }
            var payload = {
                id: contact.id ? contact.id.toString() : null,
                name: contact.name,
                last_name: contact.lastName || '',
                type: contact.type || 'NOT_SPECIFIED',
                email: contact.email || '',
                phone: contact.phone || '',
                country_code: contact.countryCode,
                phone_code: contact.phoneCode,
                street_address: contact.streetAddress || '',
                city: contact.city || '',
                state_iso: contact.stateIso || '',
                // Maybe unused
                client_id: null,
                full_name: null,
                origin: contact.origin,
                created_at: contact.createdAt,
                updated_at: contact.updatedAt
            };
            excludedFields.forEach(function (f) {
                if (payload.hasOwnProperty(f)) {
                    delete payload[f];
                }
            });
            return payload;
        };
        ContactModel.fromDataResponse = function (data) {
            return new ContactModel({
                id: +data.id,
                name: data.name,
                lastName: data.last_name,
                type: data.type,
                origin: data.origin,
                email: data.email,
                phone: data.phone,
                countryCode: data.country_code,
                phoneCode: data.phone_code,
                streetAddress: data.street_address,
                city: data.city,
                stateIso: data.state_iso,
                createdAt: data.created_at,
                updatedAt: data.updated_at
            });
        };
        ContactModel.fromContactForm = function (form) {
            return new ContactModel({
                // Required
                id: +form.id,
                name: form.name,
                countryCode: form.country_code,
                phoneCode: form.phone_code,
                lastName: form.last_name,
                type: form.type,
                phone: form.phone,
                email: form.email,
                streetAddress: form.street_address,
                city: form.city,
                stateIso: form.state_iso
            });
        };
        ContactModel.empty = function () {
            return new ContactModel({
                id: null,
                name: '',
                countryCode: null,
                phoneCode: null
            });
        };
        return ContactModel;
    }());
    var ContactInteractionModel = /** @class */ (function () {
        function ContactInteractionModel(data) {
            this.id = data.id;
            this.contactId = data.contactId;
            this.entity = data.entity;
            this.entityId = data.entityId;
            this.actionType = data.actionType;
            this.createdAt = data.createdAt;
            this.displayText = data.displayText || '';
        }
        ContactInteractionModel.fromDataResponse = function (data) {
            return new ContactInteractionModel({
                id: +data.id,
                contactId: +data.contact_id,
                entity: data.entity,
                entityId: +data.entity_id,
                actionType: data.action_type,
                createdAt: data.created_at,
                displayText: data.display_text,
            });
        };
        ContactInteractionModel.empty = function () {
            return new ContactInteractionModel({
                id: null,
                contactId: null,
                entity: '',
                entityId: null,
                actionType: null,
                createdAt: null,
                displayText: null
            });
        };
        return ContactInteractionModel;
    }());
    /*export enum ContactInteractionTypes{
        CREATE,
        UPDATE_TYPE,
        WEBSITE_FORM,
        EVENT_INVITE,
        CREATE_NOTE,
        CONTACT_EMAIL,
        CONTACT_CALL,
        CONTACT_WHATSAPP
    }*/


    (function (ContactsActionTypes) {
        ContactsActionTypes["FetchContactBegin"] = "[Contacs] Fetch contact begin";
        ContactsActionTypes["FetchContactSuccess"] = "[Contacs] Fetch contact success";
        ContactsActionTypes["FetchContactFail"] = "[Contacs] Fetch contact failure";
        ContactsActionTypes["FetchInteractionsBegin"] = "[Contacs] Fetch Interactions begin";
        ContactsActionTypes["FetchInteractionsSuccess"] = "[Contacs] Fetch Interactions success";
        ContactsActionTypes["FetchInteractionsFail"] = "[Contacs] Fetch Interactions failure";
        ContactsActionTypes["CreateContactBegin"] = "[Contacs] Create begin";
        ContactsActionTypes["CreateContactSuccess"] = "[Contacs] Create success";
        ContactsActionTypes["CreateContactFail"] = "[Contacs] Create failure";
        ContactsActionTypes["DeleteContactBegin"] = "[Contacs] Delete contact begin";
        ContactsActionTypes["DeleteContactSuccess"] = "[Contacs] Delete contact success";
        ContactsActionTypes["DeleteContactFail"] = "[Contacs] Delete contact failure";
        ContactsActionTypes["UpdateContactBegin"] = "[Contacs] Update contact begin";
        ContactsActionTypes["UpdateContactSuccess"] = "[Contacs] Update contact success";
        ContactsActionTypes["UpdateContactFail"] = "[Contacs] Update contact failure";
        ContactsActionTypes["ImportContactBegin"] = "[Contacs] Import contact begin";
        ContactsActionTypes["ImportContactSuccess"] = "[Contacs] Import contact success";
        ContactsActionTypes["ImportContactFail"] = "[Contacs] Import contact failure";
        ContactsActionTypes["FilterContactsBegin"] = "[Contacs] Filter contact begin";
        ContactsActionTypes["FilterContactsSuccess"] = "[Contacs] Filter contact success";
        ContactsActionTypes["FilterContactsFail"] = "[Contacs] Filter contact failure";
        ContactsActionTypes["SelectContact"] = "[Contacs] Select contact";
        ContactsActionTypes["FetchCountryCodesBegin"] = "[Contacs] Fetch Country Codes Begin";
        ContactsActionTypes["FetchCountryCodesSuccess"] = "[Contacs] Fetch Country Codes Success";
        ContactsActionTypes["FetchCountryCodesFail"] = "[Contacs] Fetch Country Codes Fail";
        ContactsActionTypes["CreateInteractionBegin"] = "[Contacs] Create Interacion Begin";
        ContactsActionTypes["CreateInteractionSuccess"] = "[Contacs] Create Interacion Success";
        ContactsActionTypes["CreateInteractionFail"] = "[Contacs] Create Interacion Fail";
    })(exports.ContactsActionTypes || (exports.ContactsActionTypes = {}));
    // Fetch contacts from remote API
    var FetchContactBeginAction = store.createAction(exports.ContactsActionTypes.FetchContactBegin);
    var FetchContactSuccessAction = store.createAction(exports.ContactsActionTypes.FetchContactSuccess, store.props());
    var FetchContactFailAction = store.createAction(exports.ContactsActionTypes.FetchContactFail, store.props());
    var FetchInteractionsBeginAction = store.createAction(exports.ContactsActionTypes.FetchInteractionsBegin, store.props());
    var FetchInteractionsSuccessAction = store.createAction(exports.ContactsActionTypes.FetchInteractionsSuccess, store.props());
    var FetchInteractionsFailAction = store.createAction(exports.ContactsActionTypes.FetchInteractionsFail, store.props());
    // Create contacts
    var CreateContactBeginAction = store.createAction(exports.ContactsActionTypes.CreateContactBegin, store.props());
    var CreateContactSuccessAction = store.createAction(exports.ContactsActionTypes.CreateContactSuccess, store.props());
    var CreateContactFailAction = store.createAction(exports.ContactsActionTypes.CreateContactFail, store.props());
    // Delete contacts
    var DeleteContactBeginAction = store.createAction(exports.ContactsActionTypes.DeleteContactBegin, store.props());
    var DeleteContactSuccessAction = store.createAction(exports.ContactsActionTypes.DeleteContactSuccess, store.props());
    var DeleteContactFailAction = store.createAction(exports.ContactsActionTypes.DeleteContactFail, store.props());
    // Update contacts
    var UpdateContactBeginAction = store.createAction(exports.ContactsActionTypes.UpdateContactBegin, store.props());
    var UpdateContactSuccessAction = store.createAction(exports.ContactsActionTypes.UpdateContactSuccess, store.props());
    var UpdateContactFailAction = store.createAction(exports.ContactsActionTypes.UpdateContactFail, store.props());
    // Import contacts
    var ImportContactBeginAction = store.createAction(exports.ContactsActionTypes.ImportContactBegin, store.props());
    var ImportContactSuccessAction = store.createAction(exports.ContactsActionTypes.ImportContactSuccess, store.props());
    var ImportContactFailAction = store.createAction(exports.ContactsActionTypes.ImportContactFail, store.props());
    // FILTERING
    var FilterContactsBeginAction = store.createAction(exports.ContactsActionTypes.FilterContactsBegin, store.props());
    var FilterContactsSuccessAction = store.createAction(exports.ContactsActionTypes.FilterContactsSuccess, store.props());
    var FilterContactsFailAction = store.createAction(exports.ContactsActionTypes.FilterContactsFail, store.props());
    var SelectContactAction = store.createAction(exports.ContactsActionTypes.SelectContact, store.props());
    var FetchCountryCodesBeginAction = store.createAction(exports.ContactsActionTypes.FetchCountryCodesBegin);
    var FetchCountryCodesSuccessAction = store.createAction(exports.ContactsActionTypes.FetchCountryCodesSuccess, store.props());
    var FetchCountryCodesFailAction = store.createAction(exports.ContactsActionTypes.FetchCountryCodesFail, store.props());
    var CreateInteractionBeginAction = store.createAction(exports.ContactsActionTypes.CreateInteractionBegin, store.props());
    var CreateInteractionSuccessAction = store.createAction(exports.ContactsActionTypes.CreateInteractionSuccess, store.props());
    var CreateInteractionFailAction = store.createAction(exports.ContactsActionTypes.CreateInteractionFail, store.props());

    var getContactState = store.createFeatureSelector('contacts');
    var ɵ0 = function (state) { return state; };
    var getContactPageState = store.createSelector(getContactState, ɵ0);
    var stateGetIsLoading = function (state) { return state.isLoading; };
    var stateGetItems = function (state) { return state.items; };
    var stateGetIsLoadingInteractions = function (state) { return state.interactions.isLoading; };
    var stateGetInteractionItems = function (state) { return state.interactions.items; };
    var stateGetFilteredItems = function (state) { return state.filteredItems; };
    var stateHasBeenFetched = function (state) { return state.hasBeenFetched; };
    var getIsLoading = store.createSelector(getContactPageState, stateGetIsLoading);
    var ɵ1 = function (state) { return state.error; };
    var getError = store.createSelector(getContactPageState, ɵ1);
    var ɵ2 = function (state) { return state.success; };
    var getSuccess = store.createSelector(getContactPageState, ɵ2);
    var getContactItems = store.createSelector(getContactPageState, stateGetItems);
    var getIsLoadingInteractions = store.createSelector(getContactPageState, stateGetIsLoadingInteractions);
    var getContactInteractionItems = store.createSelector(getContactPageState, stateGetInteractionItems);
    var ɵ3 = function (state) { return state.interactions.error; };
    var getContactInteractionError = store.createSelector(getContactPageState, ɵ3);
    var getFilteredContactItems = store.createSelector(getContactPageState, stateGetFilteredItems);
    var ɵ4 = function (state) { return state.countryCodes.isLoading; };
    var getIsLoadingCountryCodes = store.createSelector(getContactPageState, ɵ4);
    var ɵ5 = function (state) { return state.countryCodes.items; };
    var getCountryCodes = store.createSelector(getContactPageState, ɵ5);
    var ɵ6 = function (state) { return state.countryCodes.error; };
    var getCountryCodesErrors = store.createSelector(getContactPageState, ɵ6);
    var ɵ7 = function (state) { return state.items.filter(function (c) { return +c.id === state.selectedId; })[0]; };
    var getContactById = store.createSelector(getContactPageState, ɵ7);
    var hasBeenFetched = store.createSelector(getContactPageState, stateHasBeenFetched);

    var ContactStore = /** @class */ (function () {
        function ContactStore(store) {
            this.store = store;
        }
        Object.defineProperty(ContactStore.prototype, "Contacts$", {
            get: function () {
                return this.store.select(getContactItems);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "Error$", {
            get: function () {
                return this.store.select(getError);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "Success$", {
            get: function () {
                return this.store.select(getSuccess);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "Interactions$", {
            get: function () {
                return this.store.select(getContactInteractionItems);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "InteractionsError$", {
            get: function () {
                return this.store.select(getContactInteractionError);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "InteractionsSuccess$", {
            get: function () {
                return this.store.select(getSuccess);
            },
            enumerable: true,
            configurable: true
        });
        ContactStore.prototype.ContactById$ = function (id) {
            this.store.dispatch(SelectContactAction({ contactId: id }));
            return this.store.select(getContactById);
        };
        Object.defineProperty(ContactStore.prototype, "Loading$", {
            get: function () {
                return this.store.select(getIsLoading);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "LoadingInteractions$", {
            get: function () {
                return this.store.select(getIsLoadingInteractions);
            },
            enumerable: true,
            configurable: true
        });
        ContactStore.prototype.fetchContacts = function () {
            this.store.dispatch(FetchContactBeginAction());
        };
        ContactStore.prototype.fetchInteractions$ = function (contactId) {
            this.store.dispatch(FetchInteractionsBeginAction({ contactId: contactId }));
        };
        ContactStore.prototype.filterContacts = function (criteria) {
            this.store.dispatch(FilterContactsBeginAction({ clientType: criteria }));
        };
        Object.defineProperty(ContactStore.prototype, "FilteredContacts$", {
            get: function () {
                return this.store.select(getFilteredContactItems);
            },
            enumerable: true,
            configurable: true
        });
        ContactStore.prototype.importContacts = function (contactList) {
            this.store.dispatch(ImportContactBeginAction({ contactList: contactList }));
        };
        ContactStore.prototype.createContact = function (contactForm) {
            this.store.dispatch(CreateContactBeginAction({ contactForm: contactForm }));
        };
        ContactStore.prototype.deleteContact = function (contactId) {
            this.store.dispatch(DeleteContactBeginAction({ contactId: contactId }));
        };
        ContactStore.prototype.updateContact = function (contactForm) {
            this.store.dispatch(UpdateContactBeginAction({ contactForm: contactForm }));
        };
        Object.defineProperty(ContactStore.prototype, "HasBeenFetched$", {
            get: function () {
                return this.store.select(hasBeenFetched);
            },
            enumerable: true,
            configurable: true
        });
        ContactStore.prototype.fetchCountryCodes = function () {
            this.store.dispatch(FetchCountryCodesBeginAction());
        };
        Object.defineProperty(ContactStore.prototype, "LoadingCodes$", {
            get: function () {
                return this.store.select(getIsLoadingCountryCodes);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "LoadinCodesErrors$", {
            get: function () {
                return this.store.select(getCountryCodesErrors);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContactStore.prototype, "CountryCodes$", {
            get: function () {
                return this.store.select(getCountryCodes);
            },
            enumerable: true,
            configurable: true
        });
        ContactStore.prototype.createInteraction = function (contactId, config) {
            this.store.dispatch(CreateInteractionBeginAction({ contactId: contactId, config: config }));
        };
        ContactStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        ContactStore = __decorate([
            core.Injectable()
        ], ContactStore);
        return ContactStore;
    }());

    var ContactsService = /** @class */ (function () {
        function ContactsService(repository, nativeContacts, store) {
            this.repository = repository;
            this.nativeContacts = nativeContacts;
            this.store = store;
        }
        ContactsService.prototype.loadRawNativeContacts = function (filter) {
            var _this = this;
            var options = new contacts.ContactFindOptions();
            options.multiple = true;
            options.filter = filter;
            var fields = ['displayName', 'name', 'phoneNumbers'];
            var observer = new rxjs.Observable(function (subscriber) {
                try {
                    _this.nativeContacts.find(fields, options).then(function (contacts) {
                        subscriber.next(contacts);
                    }, function (error) {
                        console.error('*** Device contacts error:', error);
                        subscriber.error(error);
                    });
                }
                catch (e) {
                    console.error('*** TRY/CATCH:: loadRawNativeContacts: Device contacts error:', e.message);
                    subscriber.error('The Contacts Plugin is not installed');
                }
            });
            return observer;
        };
        ContactsService.prototype.pickOne = function (phone) {
            var _this = this;
            var options = new contacts.ContactFindOptions();
            options.multiple = true;
            var fields = ['phoneNumbers'];
            return new rxjs.Observable(function (subscriber) {
                try {
                    _this.nativeContacts.find(fields, options).then(function (contacts) {
                        var res = contacts.filter(function (c) { return c.phoneNumbers.some(function (p) { return p.value.replace(/[^0-9]+/g, '') === phone; }); });
                        subscriber.next(res.length > 0 ? res[0] : null);
                    }, function (error) {
                        console.error('*** pickOne Device: contacts error:', error);
                        subscriber.error(error);
                    });
                }
                catch (e) {
                    console.error('*** TRY/CATCH:: pickOne: Device contacts error:', e.message);
                    subscriber.error('The Contacts Plugin is not installed');
                }
            });
        };
        ContactsService.prototype.loadFormattedNativeContacts = function (filter) {
            var _this = this;
            var observer = new rxjs.Observable(function (subscriber) {
                _this.loadRawNativeContacts(filter)
                    .pipe(operators.withLatestFrom(_this.store.Contacts$))
                    .subscribe(function (_a) {
                    var _b = __read(_a, 2), contacts = _b[0], stored = _b[1];
                    var formattedContacts = contacts
                        .filter(function (contact) {
                        contact.name = contact.name || { givenName: '', familyName: '' };
                        return contact.name.givenName
                            && stored.findIndex(function (s) { return s.phone === (contact.phoneNumbers ? contact.phoneNumbers[0].value : ''); }) === -1;
                    })
                        .sort(function (a, b) { return a.name.givenName.localeCompare(b.name.givenName); })
                        .map(function (c) { return new ContactModel({
                        id: null,
                        name: c.name ? c.name.givenName : '',
                        lastName: c.name ? c.name.familyName : '',
                        type: 'NOT_SPECIFIED',
                        email: c.emails ? c.emails[0].value : '',
                        phone: c.phoneNumbers ? c.phoneNumbers[0].value : '',
                    }); });
                    subscriber.next(formattedContacts);
                }, function (error) {
                    console.error('****** loadFormatted Device contacts error:', error);
                    subscriber.error(error);
                });
            });
            return observer;
        };
        ContactsService.prototype.loadRemoteContacts = function () {
            return this.repository.getContacts().pipe(operators.map(function (response) {
                return response.data.map(function (contact) {
                    var contactModel = ContactModel.fromDataResponse(contact);
                    return contactModel;
                }).sort(function (a, b) { return b.id - a.id; });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.loadCountryCodes = function () {
            return this.repository.getCountryCodes().pipe(operators.map(function (response) {
                return (response && response.length) ? response.sort(function (a, b) { return a.name.localeCompare(b.name); }) : COUNTRY_CALLING_CODES;
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.loadContactInteractions = function (contactId) {
            return this.repository.getContactInteractions(contactId).pipe(operators.map(function (response) {
                return response.data.map(function (interaction) {
                    return ContactInteractionModel.fromDataResponse(interaction);
                });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.createContact = function (form) {
            return this.repository.createContact(form).pipe(operators.map(function (response) {
                if (response.data.id) {
                    return ContactModel.fromDataResponse(response.data);
                }
                else {
                    throw new Error('Unknown error');
                }
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.deleteContact = function (contactId) {
            return this.repository.deleteContact(contactId).pipe(operators.map(function (response) {
                if (response.status === 'success') {
                    return contactId;
                }
                else {
                    throw { statusCode: 500, status: 'error' };
                }
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.updateContact = function (form) {
            return this.repository.updateContact(form).pipe(operators.map(function (response) {
                return ContactModel.fromDataResponse(response.data);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.importContacts = function (contactList) {
            return this.repository.importContacts(contactList).pipe(operators.map(function (response) {
                return response.data.contacts_exported.map(function (c) { return ContactModel.fromDataResponse(c); });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.prototype.createInteraction = function (contactId, config) {
            return this.repository.createInteraction(contactId, config).pipe(operators.map(function (response) {
                return ContactInteractionModel.fromDataResponse(response.data);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ContactsService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [CONTACTS_REPOSITORY,] }] },
            { type: contacts.Contacts },
            { type: ContactStore }
        ]; };
        ContactsService.ɵprov = core.ɵɵdefineInjectable({ factory: function ContactsService_Factory() { return new ContactsService(core.ɵɵinject(CONTACTS_REPOSITORY), core.ɵɵinject(contacts.Contacts), core.ɵɵinject(ContactStore)); }, token: ContactsService, providedIn: "root" });
        ContactsService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(CONTACTS_REPOSITORY))
        ], ContactsService);
        return ContactsService;
    }());

    var CONTACTS_SERVICE = new core.InjectionToken('contactsService');

    var ContactsEffects = /** @class */ (function () {
        function ContactsEffects(actions$, store, service) {
            var _this = this;
            this.actions$ = actions$;
            this.store = store;
            this.service = service;
            this.load$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.FetchContactBegin), operators.switchMap(function () {
                return _this.service.loadRemoteContacts().pipe(operators.map(function (contacList) { return FetchContactSuccessAction({ contacList: contacList }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t fetch contacts');
                    return rxjs.of(FetchContactFailAction({ errors: error }));
                }));
            })); });
            this.loadCountryCodes$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.FetchCountryCodesBegin), operators.switchMap(function () {
                return _this.service.loadCountryCodes().pipe(operators.map(function (codes) { return FetchCountryCodesSuccessAction({ codes: codes }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t fetch country codes', error);
                    return rxjs.of(FetchCountryCodesFailAction({ errors: error }));
                }));
            })); });
            this.loadInteractions$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.FetchInteractionsBegin), operators.switchMap(function (action) {
                return _this.service.loadContactInteractions(action.contactId).pipe(operators.map(function (interactions) { return FetchInteractionsSuccessAction({ interactions: interactions }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t fetch contact interactions');
                    return rxjs.of(FetchInteractionsFailAction({ errors: error }));
                }));
            })); });
            this.create$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.CreateContactBegin), operators.switchMap(function (action) {
                return _this.service.createContact(action.contactForm).pipe(operators.map(function (contact) { return CreateContactSuccessAction({ contact: contact }); }), operators.catchError(function (errors) {
                    console.error('Couldn\'t Create contact');
                    return rxjs.of(CreateContactFailAction({ errors: errors }));
                }));
            })); });
            this.delete$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.DeleteContactBegin), operators.switchMap(function (action) { return _this.service.deleteContact(action.contactId).pipe(operators.map(function (contactId) { return DeleteContactSuccessAction({ contactId: contactId }); }), operators.catchError(function (errors) {
                console.error('Couldn\'t Delete contact');
                return rxjs.of(DeleteContactFailAction({ errors: errors }));
            })); })); });
            this.update$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.UpdateContactBegin), operators.switchMap(function (action) { return _this.service.updateContact(action.contactForm).pipe(operators.map(function (updatedContact) { return UpdateContactSuccessAction({ contact: updatedContact }); }), operators.catchError(function (errors) {
                console.error('Couldn\'t Update contact');
                return rxjs.of(UpdateContactFailAction({ errors: errors }));
            })); })); });
            this.import$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.ImportContactBegin), operators.switchMap(function (action) {
                return _this.service.importContacts(action.contactList).pipe(operators.map(function (contactList) {
                    return ImportContactSuccessAction({ contactList: contactList });
                }), operators.catchError(function (errors) {
                    return rxjs.of(ImportContactFailAction({ errors: errors }));
                }));
            })); });
            this.filter$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.FilterContactsBegin), operators.withLatestFrom(_this.store.Contacts$), operators.switchMap(function (_a) {
                var _b = __read(_a, 2), action = _b[0], storedContacts = _b[1];
                var contactList = action.clientType === 'ALL' ? storedContacts :
                    storedContacts.filter(function (item) { return item.type === action.clientType; });
                return rxjs.of(FilterContactsSuccessAction({ contactList: contactList }));
            }), operators.catchError(function (errors) {
                console.error('**** Filter contacts Error');
                return rxjs.of(FilterContactsFailAction({ errors: errors }));
            })); });
            this.createInteraction$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ContactsActionTypes.CreateInteractionBegin), operators.switchMap(function (action) {
                return _this.service.createInteraction(action.contactId, action.config).pipe(operators.map(function (interaction) {
                    return CreateInteractionSuccessAction({ interaction: interaction });
                }), operators.catchError(function (errors) {
                    return rxjs.of(CreateInteractionFailAction({ errors: errors }));
                }));
            })); });
        }
        ContactsEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: ContactStore },
            { type: undefined, decorators: [{ type: core.Inject, args: [CONTACTS_SERVICE,] }] }
        ]; };
        ContactsEffects = __decorate([
            core.Injectable(),
            __param(2, core.Inject(CONTACTS_SERVICE))
        ], ContactsEffects);
        return ContactsEffects;
    }());

    var initialState = {
        isLoading: false,
        items: [],
        filteredItems: [],
        selectedId: null,
        interactions: {
            isLoading: false,
            items: [],
            error: null
        },
        countryCodes: {
            items: [],
            isLoading: false,
            error: null
        },
        hasBeenFetched: false,
        error: null,
        success: null
    };
    var ɵ0$1 = function (state) { return (__assign(__assign({}, state), { isLoading: true, error: null, success: null })); }, ɵ1$1 = function (state) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { error: null, isLoading: true }) })); }, ɵ2$1 = function (state) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: true, items: [], error: null }) })); }, ɵ3$1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ4$1 = function (state, action) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ5$1 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ6$1 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ7$1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: action.contacList, hasBeenFetched: true, success: { after: getSuccessActionType(action.type) } })); }, ɵ8 = function (state, action) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { items: action.codes, isLoading: false }) })); }, ɵ9 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, items: action.interactions }) })); }, ɵ10 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, items: __spread([action.interaction], state.interactions.items) }) })); }, ɵ11 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread([action.contact], state.items), success: { after: getSuccessActionType(action.type) } })); }, ɵ12 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(action.contactList, state.items), success: { after: getSuccessActionType(action.type) } })); }, ɵ13 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(state.items.filter(function (c) { return c.id !== action.contactId; })), success: { after: getSuccessActionType(action.type) } })); }, ɵ14 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread((function (cl) {
            var tmp = __spread(cl);
            var idx = cl.findIndex(function (m) { return m.id === action.contact.id; });
            if (idx !== -1) {
                tmp.splice(idx, 1, action.contact);
            }
            return tmp;
        })(state.items)), success: { after: getSuccessActionType(action.type) } })); }, ɵ15 = function (state, action) { return (__assign(__assign({}, state), { filteredItems: action.contactList, isLoading: false, error: null, success: null })); }, ɵ16 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.contactId, error: null, success: null })); };
    var reducer = store.createReducer(initialState, 
    // On Begin Actions
    store.on(FetchContactBeginAction, CreateContactBeginAction, DeleteContactBeginAction, UpdateContactBeginAction, ImportContactBeginAction, ɵ0$1), store.on(FetchCountryCodesBeginAction, ɵ1$1), store.on(FetchInteractionsBeginAction, ɵ2$1), 
    // ON Fail Actions
    store.on(FetchContactFailAction, CreateContactFailAction, DeleteContactFailAction, UpdateContactFailAction, ImportContactFailAction, ɵ3$1), store.on(FetchCountryCodesFailAction, ɵ4$1), store.on(FetchInteractionsFailAction, ɵ5$1), store.on(CreateInteractionFailAction, ɵ6$1), 
    // ON Success Actions:
    // FETCH
    store.on(FetchContactSuccessAction, ɵ7$1), store.on(FetchCountryCodesSuccessAction, ɵ8), store.on(FetchInteractionsSuccessAction, ɵ9), store.on(CreateInteractionSuccessAction, ɵ10), 
    // INSERT Contacts:
    store.on(CreateContactSuccessAction, ɵ11), store.on(ImportContactSuccessAction, ɵ12), 
    // REMOVES Contacts
    store.on(DeleteContactSuccessAction, ɵ13), 
    // UPDATES Contacts
    store.on(UpdateContactSuccessAction, ɵ14), 
    // FILTER
    store.on(FilterContactsSuccessAction, ɵ15), 
    // SELECT
    store.on(SelectContactAction, ɵ16));
    function getErrorActionType(type) {
        var action = 'UNKNOWN';
        switch (type) {
            case exports.ContactsActionTypes.FetchContactFail:
            case exports.ContactsActionTypes.FetchCountryCodesFail:
                action = 'GET';
                break;
            case exports.ContactsActionTypes.FetchInteractionsFail:
                action = 'GET_INTERACTIONS';
                break;
            case exports.ContactsActionTypes.CreateContactFail:
            case exports.ContactsActionTypes.CreateInteractionFail:
                action = 'CREATE';
                break;
            case exports.ContactsActionTypes.UpdateContactFail:
                action = 'UPDATE';
                break;
            case exports.ContactsActionTypes.DeleteContactFail:
                action = 'DELETE';
                break;
            case exports.ContactsActionTypes.ImportContactFail:
                action = 'IMPORT';
                break;
        }
        return action;
    }
    function getSuccessActionType(type) {
        var action = 'UNKNOWN';
        switch (type) {
            case exports.ContactsActionTypes.FetchContactSuccess:
                action = 'GET';
                break;
            case exports.ContactsActionTypes.CreateContactSuccess:
                action = 'CREATE';
                break;
            case exports.ContactsActionTypes.UpdateContactSuccess:
                action = 'UPDATE';
                break;
            case exports.ContactsActionTypes.DeleteContactSuccess:
                action = 'DELETE';
                break;
            case exports.ContactsActionTypes.ImportContactSuccess:
                action = 'IMPORT';
                break;
        }
        return action;
    }
    function contactsReducer(state, action) {
        return reducer(state, action);
    }

    var ContactsCoreModule = /** @class */ (function () {
        function ContactsCoreModule() {
        }
        ContactsCoreModule_1 = ContactsCoreModule;
        ContactsCoreModule.forRoot = function (config) {
            return {
                ngModule: ContactsCoreModule_1,
                providers: __spread([
                    { provide: CONTACTS_SERVICE, useClass: ContactsService },
                    { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository }
                ], config.providers, [
                    contacts.Contacts,
                    ContactStore
                ])
            };
        };
        var ContactsCoreModule_1;
        ContactsCoreModule = ContactsCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [
                    ContactItemComponent
                ],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('contacts', contactsReducer),
                    effects.EffectsModule.forFeature([ContactsEffects]),
                    common.CommonModule,
                    angular.IonicModule
                ],
                exports: [
                    ContactItemComponent
                ]
            })
        ], ContactsCoreModule);
        return ContactsCoreModule;
    }());

    exports.CONTACTS_REPOSITORY = CONTACTS_REPOSITORY;
    exports.CONTACTS_SERVICE = CONTACTS_SERVICE;
    exports.COUNTRY_CALLING_CODES = COUNTRY_CALLING_CODES;
    exports.ContactInteractionModel = ContactInteractionModel;
    exports.ContactModel = ContactModel;
    exports.ContactStore = ContactStore;
    exports.ContactsCoreModule = ContactsCoreModule;
    exports.ContactsEffects = ContactsEffects;
    exports.ContactsRepository = ContactsRepository;
    exports.ContactsService = ContactsService;
    exports.CreateContactBeginAction = CreateContactBeginAction;
    exports.CreateContactFailAction = CreateContactFailAction;
    exports.CreateContactSuccessAction = CreateContactSuccessAction;
    exports.CreateInteractionBeginAction = CreateInteractionBeginAction;
    exports.CreateInteractionFailAction = CreateInteractionFailAction;
    exports.CreateInteractionSuccessAction = CreateInteractionSuccessAction;
    exports.DeleteContactBeginAction = DeleteContactBeginAction;
    exports.DeleteContactFailAction = DeleteContactFailAction;
    exports.DeleteContactSuccessAction = DeleteContactSuccessAction;
    exports.FetchContactBeginAction = FetchContactBeginAction;
    exports.FetchContactFailAction = FetchContactFailAction;
    exports.FetchContactSuccessAction = FetchContactSuccessAction;
    exports.FetchCountryCodesBeginAction = FetchCountryCodesBeginAction;
    exports.FetchCountryCodesFailAction = FetchCountryCodesFailAction;
    exports.FetchCountryCodesSuccessAction = FetchCountryCodesSuccessAction;
    exports.FetchInteractionsBeginAction = FetchInteractionsBeginAction;
    exports.FetchInteractionsFailAction = FetchInteractionsFailAction;
    exports.FetchInteractionsSuccessAction = FetchInteractionsSuccessAction;
    exports.FilterContactsBeginAction = FilterContactsBeginAction;
    exports.FilterContactsFailAction = FilterContactsFailAction;
    exports.FilterContactsSuccessAction = FilterContactsSuccessAction;
    exports.ImportContactBeginAction = ImportContactBeginAction;
    exports.ImportContactFailAction = ImportContactFailAction;
    exports.ImportContactSuccessAction = ImportContactSuccessAction;
    exports.SelectContactAction = SelectContactAction;
    exports.UpdateContactBeginAction = UpdateContactBeginAction;
    exports.UpdateContactFailAction = UpdateContactFailAction;
    exports.UpdateContactSuccessAction = UpdateContactSuccessAction;
    exports.contactsReducer = contactsReducer;
    exports.getContactById = getContactById;
    exports.getContactInteractionError = getContactInteractionError;
    exports.getContactInteractionItems = getContactInteractionItems;
    exports.getContactItems = getContactItems;
    exports.getContactPageState = getContactPageState;
    exports.getContactState = getContactState;
    exports.getCountryCodes = getCountryCodes;
    exports.getCountryCodesErrors = getCountryCodesErrors;
    exports.getError = getError;
    exports.getFilteredContactItems = getFilteredContactItems;
    exports.getIsLoading = getIsLoading;
    exports.getIsLoadingCountryCodes = getIsLoadingCountryCodes;
    exports.getIsLoadingInteractions = getIsLoadingInteractions;
    exports.getSuccess = getSuccess;
    exports.hasBeenFetched = hasBeenFetched;
    exports.initialState = initialState;
    exports.stateGetFilteredItems = stateGetFilteredItems;
    exports.stateGetInteractionItems = stateGetInteractionItems;
    exports.stateGetIsLoading = stateGetIsLoading;
    exports.stateGetIsLoadingInteractions = stateGetIsLoadingInteractions;
    exports.stateGetItems = stateGetItems;
    exports.stateHasBeenFetched = stateHasBeenFetched;
    exports.ɵ0 = ɵ0;
    exports.ɵ1 = ɵ1;
    exports.ɵ2 = ɵ2;
    exports.ɵ3 = ɵ3;
    exports.ɵ4 = ɵ4;
    exports.ɵ5 = ɵ5;
    exports.ɵ6 = ɵ6;
    exports.ɵ7 = ɵ7;
    exports.ɵa = ContactItemComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-contacts-core.umd.js.map
