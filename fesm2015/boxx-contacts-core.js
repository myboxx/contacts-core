import { __decorate, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, Inject, Injectable, InjectionToken, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { ContactFindOptions, Contacts } from '@ionic-native/contacts';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createFeatureSelector, createSelector, Store, createReducer, on, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { Observable, of } from 'rxjs';
import { withLatestFrom, map, catchError, switchMap } from 'rxjs/operators';

let ContactItemComponent = class ContactItemComponent {
    constructor() {
        this.showDetail = true;
        this.showEmail = false;
    }
    ngOnInit() { }
};
__decorate([
    Input()
], ContactItemComponent.prototype, "showDetail", void 0);
__decorate([
    Input()
], ContactItemComponent.prototype, "contact", void 0);
__decorate([
    Input()
], ContactItemComponent.prototype, "showEmail", void 0);
ContactItemComponent = __decorate([
    Component({
        selector: 'boxx-contact-item',
        template: "<ion-item detail=\"{{showDetail}}\">\n    <ion-avatar slot=\"start\" class=\"boxx-contact-item-avatar\">\n        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n    </ion-avatar>\n    <ion-label>\n        <h2>{{contact.name}} {{contact.lastName}}</h2>\n        <p class=\"boxx-contact-item-detail email\" *ngIf=\"contact.email && showEmail\">\n            <ion-icon class=\"boxx-contact-item-detail-icon email\" name=\"mail-open\"></ion-icon>\n            {{contact.email}}\n        </p>\n        <p class=\"boxx-contact-item-detail phone\" *ngIf=\"contact.phone\">\n            <ion-icon class=\"boxx-contact-item-detail-icon phone\" name=\"call\"></ion-icon>\n            {{contact.phone}}\n        </p>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
        styles: [".contact-icon-detail{display:flex;align-items:center}.contact-icon-detail .boxx-contact-item-detail-icon{margin-right:8px}"]
    })
], ContactItemComponent);

let ContactsRepository = class ContactsRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    getContacts() {
        return this.httpClient.get(`${this.getBaseUrl()}`);
    }
    getCountryCodes() {
        return this.httpClient.get(
        // `https://restcountries.eu/rest/v2/all?fields=flag;alpha3Code;callingCodes;translations;name`
        './../assets/countryCodes.json');
    }
    getContactInteractions(contactId) {
        return this.httpClient.get(`${this.getBaseUrl()}/interactions/${contactId}`);
    }
    createContact(payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/create`, body);
    }
    deleteContact(contactId) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete/${contactId}`);
    }
    updateContact(payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/update/${payload.id}`, body);
    }
    importContacts(payload) {
        const data = { export_data: JSON.stringify({ contacts: payload }) };
        const urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach((key) => {
            urlSearchParams.append(key, data[key]);
        });
        const body = urlSearchParams.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/export_from_mobile`, body);
    }
    createInteraction(contactId, config) {
        const urlSearchParams = new URLSearchParams();
        Object.keys(config).forEach((key) => {
            urlSearchParams.append(key, config[key]);
        });
        const body = urlSearchParams.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/create_interaction/${contactId}`, body);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/contacts`;
    }
};
ContactsRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
ContactsRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], ContactsRepository);
/*export const countryCallingCodes:{[key: string]: string} = {
    //Country codes, source: https://countrycode.org

    //Note:
    //https://faq.whatsapp.com/sv/wp/21016748/?category=5245236
    //Make sure to remove any leading 0's or special calling codes
    //All phone numbers in Argentina (country code "54") should have a "9"
    // between the country code and area code. The prefix "15" must be removed so the
    // final number will have 13 digits total: +54 9 xxx xxx xxxx
    //Phone numbers in Mexico (country code "52") need to have "1" after "+52", even if they're Nextel numbers.

    'ARG': '54 9',//Argentina
    'MEX': '52 1',//México
  };*/
// Use the nex api: https://restcountries.eu/
// Example:
// https://restcountries.eu/rest/v2/{service}?fields={field};{field};{field}
// https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
// https://restcountries.eu/rest/v2/name/mexico?fields=flag;alpha3Code;callingCodes;translations
const COUNTRY_CALLING_CODES = [
    { alpha3Code: 'ARG', callingCodes: ['54'], flag: 'https://restcountries.eu/data/arg.svg', name: 'Argentina', translations: { es: 'Argentina' } },
    {
        alpha3Code: 'MEX',
        callingCodes: ['52'],
        flag: 'https://restcountries.eu/data/mex.svg',
        name: 'México', translations: { es: 'México' }
    },
    { alpha3Code: 'USA', callingCodes: ['1'], flag: 'https://restcountries.eu/data/usa.svg', name: 'Estado Unidos', translations: { es: 'Estado Unidos' } },
];

const CONTACTS_REPOSITORY = new InjectionToken('contactsRepository');

class ContactModel {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.lastName = data.lastName || '';
        this.type = data.type || 'NOT_SPECIFIED';
        this.origin = data.origin || 'UNKNOWN';
        this.email = data.email || '';
        this.phone = (data.phone || '').replace(/[^0-9]+/g, '').slice(-10);
        this.countryCode = data.countryCode || 'MEX';
        this.phoneCode = data.phoneCode || '+52';
        this.streetAddress = data.streetAddress || '';
        this.city = data.city || '';
        this.stateIso = data.stateIso || '';
        this.createdAt = data.createdAt || '';
        this.updatedAt = data.updatedAt || '';
    }
    static toApiModel(contact, excludedFields = []) {
        const payload = {
            id: contact.id ? contact.id.toString() : null,
            name: contact.name,
            last_name: contact.lastName || '',
            type: contact.type || 'NOT_SPECIFIED',
            email: contact.email || '',
            phone: contact.phone || '',
            country_code: contact.countryCode,
            phone_code: contact.phoneCode,
            street_address: contact.streetAddress || '',
            city: contact.city || '',
            state_iso: contact.stateIso || '',
            // Maybe unused
            client_id: null,
            full_name: null,
            origin: contact.origin,
            created_at: contact.createdAt,
            updated_at: contact.updatedAt
        };
        excludedFields.forEach(f => {
            if (payload.hasOwnProperty(f)) {
                delete payload[f];
            }
        });
        return payload;
    }
    static fromDataResponse(data) {
        return new ContactModel({
            id: +data.id,
            name: data.name,
            lastName: data.last_name,
            type: data.type,
            origin: data.origin,
            email: data.email,
            phone: data.phone,
            countryCode: data.country_code,
            phoneCode: data.phone_code,
            streetAddress: data.street_address,
            city: data.city,
            stateIso: data.state_iso,
            createdAt: data.created_at,
            updatedAt: data.updated_at
        });
    }
    static fromContactForm(form) {
        return new ContactModel({
            // Required
            id: +form.id,
            name: form.name,
            countryCode: form.country_code,
            phoneCode: form.phone_code,
            lastName: form.last_name,
            type: form.type,
            phone: form.phone,
            email: form.email,
            streetAddress: form.street_address,
            city: form.city,
            stateIso: form.state_iso
        });
    }
    static empty() {
        return new ContactModel({
            id: null,
            name: '',
            countryCode: null,
            phoneCode: null
        });
    }
}
class ContactInteractionModel {
    constructor(data) {
        this.id = data.id;
        this.contactId = data.contactId;
        this.entity = data.entity;
        this.entityId = data.entityId;
        this.actionType = data.actionType;
        this.createdAt = data.createdAt;
        this.displayText = data.displayText || '';
    }
    static fromDataResponse(data) {
        return new ContactInteractionModel({
            id: +data.id,
            contactId: +data.contact_id,
            entity: data.entity,
            entityId: +data.entity_id,
            actionType: data.action_type,
            createdAt: data.created_at,
            displayText: data.display_text,
        });
    }
    static empty() {
        return new ContactInteractionModel({
            id: null,
            contactId: null,
            entity: '',
            entityId: null,
            actionType: null,
            createdAt: null,
            displayText: null
        });
    }
}
/*export enum ContactInteractionTypes{
    CREATE,
    UPDATE_TYPE,
    WEBSITE_FORM,
    EVENT_INVITE,
    CREATE_NOTE,
    CONTACT_EMAIL,
    CONTACT_CALL,
    CONTACT_WHATSAPP
}*/

var ContactsActionTypes;
(function (ContactsActionTypes) {
    ContactsActionTypes["FetchContactBegin"] = "[Contacs] Fetch contact begin";
    ContactsActionTypes["FetchContactSuccess"] = "[Contacs] Fetch contact success";
    ContactsActionTypes["FetchContactFail"] = "[Contacs] Fetch contact failure";
    ContactsActionTypes["FetchInteractionsBegin"] = "[Contacs] Fetch Interactions begin";
    ContactsActionTypes["FetchInteractionsSuccess"] = "[Contacs] Fetch Interactions success";
    ContactsActionTypes["FetchInteractionsFail"] = "[Contacs] Fetch Interactions failure";
    ContactsActionTypes["CreateContactBegin"] = "[Contacs] Create begin";
    ContactsActionTypes["CreateContactSuccess"] = "[Contacs] Create success";
    ContactsActionTypes["CreateContactFail"] = "[Contacs] Create failure";
    ContactsActionTypes["DeleteContactBegin"] = "[Contacs] Delete contact begin";
    ContactsActionTypes["DeleteContactSuccess"] = "[Contacs] Delete contact success";
    ContactsActionTypes["DeleteContactFail"] = "[Contacs] Delete contact failure";
    ContactsActionTypes["UpdateContactBegin"] = "[Contacs] Update contact begin";
    ContactsActionTypes["UpdateContactSuccess"] = "[Contacs] Update contact success";
    ContactsActionTypes["UpdateContactFail"] = "[Contacs] Update contact failure";
    ContactsActionTypes["ImportContactBegin"] = "[Contacs] Import contact begin";
    ContactsActionTypes["ImportContactSuccess"] = "[Contacs] Import contact success";
    ContactsActionTypes["ImportContactFail"] = "[Contacs] Import contact failure";
    ContactsActionTypes["FilterContactsBegin"] = "[Contacs] Filter contact begin";
    ContactsActionTypes["FilterContactsSuccess"] = "[Contacs] Filter contact success";
    ContactsActionTypes["FilterContactsFail"] = "[Contacs] Filter contact failure";
    ContactsActionTypes["SelectContact"] = "[Contacs] Select contact";
    ContactsActionTypes["FetchCountryCodesBegin"] = "[Contacs] Fetch Country Codes Begin";
    ContactsActionTypes["FetchCountryCodesSuccess"] = "[Contacs] Fetch Country Codes Success";
    ContactsActionTypes["FetchCountryCodesFail"] = "[Contacs] Fetch Country Codes Fail";
    ContactsActionTypes["CreateInteractionBegin"] = "[Contacs] Create Interacion Begin";
    ContactsActionTypes["CreateInteractionSuccess"] = "[Contacs] Create Interacion Success";
    ContactsActionTypes["CreateInteractionFail"] = "[Contacs] Create Interacion Fail";
})(ContactsActionTypes || (ContactsActionTypes = {}));
// Fetch contacts from remote API
const FetchContactBeginAction = createAction(ContactsActionTypes.FetchContactBegin);
const FetchContactSuccessAction = createAction(ContactsActionTypes.FetchContactSuccess, props());
const FetchContactFailAction = createAction(ContactsActionTypes.FetchContactFail, props());
const FetchInteractionsBeginAction = createAction(ContactsActionTypes.FetchInteractionsBegin, props());
const FetchInteractionsSuccessAction = createAction(ContactsActionTypes.FetchInteractionsSuccess, props());
const FetchInteractionsFailAction = createAction(ContactsActionTypes.FetchInteractionsFail, props());
// Create contacts
const CreateContactBeginAction = createAction(ContactsActionTypes.CreateContactBegin, props());
const CreateContactSuccessAction = createAction(ContactsActionTypes.CreateContactSuccess, props());
const CreateContactFailAction = createAction(ContactsActionTypes.CreateContactFail, props());
// Delete contacts
const DeleteContactBeginAction = createAction(ContactsActionTypes.DeleteContactBegin, props());
const DeleteContactSuccessAction = createAction(ContactsActionTypes.DeleteContactSuccess, props());
const DeleteContactFailAction = createAction(ContactsActionTypes.DeleteContactFail, props());
// Update contacts
const UpdateContactBeginAction = createAction(ContactsActionTypes.UpdateContactBegin, props());
const UpdateContactSuccessAction = createAction(ContactsActionTypes.UpdateContactSuccess, props());
const UpdateContactFailAction = createAction(ContactsActionTypes.UpdateContactFail, props());
// Import contacts
const ImportContactBeginAction = createAction(ContactsActionTypes.ImportContactBegin, props());
const ImportContactSuccessAction = createAction(ContactsActionTypes.ImportContactSuccess, props());
const ImportContactFailAction = createAction(ContactsActionTypes.ImportContactFail, props());
// FILTERING
const FilterContactsBeginAction = createAction(ContactsActionTypes.FilterContactsBegin, props());
const FilterContactsSuccessAction = createAction(ContactsActionTypes.FilterContactsSuccess, props());
const FilterContactsFailAction = createAction(ContactsActionTypes.FilterContactsFail, props());
const SelectContactAction = createAction(ContactsActionTypes.SelectContact, props());
const FetchCountryCodesBeginAction = createAction(ContactsActionTypes.FetchCountryCodesBegin);
const FetchCountryCodesSuccessAction = createAction(ContactsActionTypes.FetchCountryCodesSuccess, props());
const FetchCountryCodesFailAction = createAction(ContactsActionTypes.FetchCountryCodesFail, props());
const CreateInteractionBeginAction = createAction(ContactsActionTypes.CreateInteractionBegin, props());
const CreateInteractionSuccessAction = createAction(ContactsActionTypes.CreateInteractionSuccess, props());
const CreateInteractionFailAction = createAction(ContactsActionTypes.CreateInteractionFail, props());

const getContactState = createFeatureSelector('contacts');
const ɵ0 = state => state;
const getContactPageState = createSelector(getContactState, ɵ0);
const stateGetIsLoading = (state) => state.isLoading;
const stateGetItems = (state) => state.items;
const stateGetIsLoadingInteractions = (state) => state.interactions.isLoading;
const stateGetInteractionItems = (state) => state.interactions.items;
const stateGetFilteredItems = (state) => state.filteredItems;
const stateHasBeenFetched = (state) => state.hasBeenFetched;
const getIsLoading = createSelector(getContactPageState, stateGetIsLoading);
const ɵ1 = state => state.error;
const getError = createSelector(getContactPageState, ɵ1);
const ɵ2 = state => state.success;
const getSuccess = createSelector(getContactPageState, ɵ2);
const getContactItems = createSelector(getContactPageState, stateGetItems);
const getIsLoadingInteractions = createSelector(getContactPageState, stateGetIsLoadingInteractions);
const getContactInteractionItems = createSelector(getContactPageState, stateGetInteractionItems);
const ɵ3 = state => state.interactions.error;
const getContactInteractionError = createSelector(getContactPageState, ɵ3);
const getFilteredContactItems = createSelector(getContactPageState, stateGetFilteredItems);
const ɵ4 = state => state.countryCodes.isLoading;
const getIsLoadingCountryCodes = createSelector(getContactPageState, ɵ4);
const ɵ5 = state => state.countryCodes.items;
const getCountryCodes = createSelector(getContactPageState, ɵ5);
const ɵ6 = state => state.countryCodes.error;
const getCountryCodesErrors = createSelector(getContactPageState, ɵ6);
const ɵ7 = state => state.items.filter(c => +c.id === state.selectedId)[0];
const getContactById = createSelector(getContactPageState, ɵ7);
const hasBeenFetched = createSelector(getContactPageState, stateHasBeenFetched);

let ContactStore = class ContactStore {
    constructor(store) {
        this.store = store;
    }
    get Contacts$() {
        return this.store.select(getContactItems);
    }
    get Error$() {
        return this.store.select(getError);
    }
    get Success$() {
        return this.store.select(getSuccess);
    }
    get Interactions$() {
        return this.store.select(getContactInteractionItems);
    }
    get InteractionsError$() {
        return this.store.select(getContactInteractionError);
    }
    get InteractionsSuccess$() {
        return this.store.select(getSuccess);
    }
    ContactById$(id) {
        this.store.dispatch(SelectContactAction({ contactId: id }));
        return this.store.select(getContactById);
    }
    get Loading$() {
        return this.store.select(getIsLoading);
    }
    get LoadingInteractions$() {
        return this.store.select(getIsLoadingInteractions);
    }
    fetchContacts() {
        this.store.dispatch(FetchContactBeginAction());
    }
    fetchInteractions$(contactId) {
        this.store.dispatch(FetchInteractionsBeginAction({ contactId }));
    }
    filterContacts(criteria) {
        this.store.dispatch(FilterContactsBeginAction({ clientType: criteria }));
    }
    get FilteredContacts$() {
        return this.store.select(getFilteredContactItems);
    }
    importContacts(contactList) {
        this.store.dispatch(ImportContactBeginAction({ contactList }));
    }
    createContact(contactForm) {
        this.store.dispatch(CreateContactBeginAction({ contactForm }));
    }
    deleteContact(contactId) {
        this.store.dispatch(DeleteContactBeginAction({ contactId }));
    }
    updateContact(contactForm) {
        this.store.dispatch(UpdateContactBeginAction({ contactForm }));
    }
    get HasBeenFetched$() {
        return this.store.select(hasBeenFetched);
    }
    fetchCountryCodes() {
        this.store.dispatch(FetchCountryCodesBeginAction());
    }
    get LoadingCodes$() {
        return this.store.select(getIsLoadingCountryCodes);
    }
    get LoadinCodesErrors$() {
        return this.store.select(getCountryCodesErrors);
    }
    get CountryCodes$() {
        return this.store.select(getCountryCodes);
    }
    createInteraction(contactId, config) {
        this.store.dispatch(CreateInteractionBeginAction({ contactId, config }));
    }
};
ContactStore.ctorParameters = () => [
    { type: Store }
];
ContactStore = __decorate([
    Injectable()
], ContactStore);

let ContactsService = class ContactsService {
    constructor(repository, nativeContacts, store) {
        this.repository = repository;
        this.nativeContacts = nativeContacts;
        this.store = store;
    }
    loadRawNativeContacts(filter) {
        const options = new ContactFindOptions();
        options.multiple = true;
        options.filter = filter;
        const fields = ['displayName', 'name', 'phoneNumbers'];
        const observer = new Observable(subscriber => {
            try {
                this.nativeContacts.find(fields, options).then((contacts) => {
                    subscriber.next(contacts);
                }, (error) => {
                    console.error('*** Device contacts error:', error);
                    subscriber.error(error);
                });
            }
            catch (e) {
                console.error('*** TRY/CATCH:: loadRawNativeContacts: Device contacts error:', e.message);
                subscriber.error('The Contacts Plugin is not installed');
            }
        });
        return observer;
    }
    pickOne(phone) {
        const options = new ContactFindOptions();
        options.multiple = true;
        const fields = ['phoneNumbers'];
        return new Observable((subscriber) => {
            try {
                this.nativeContacts.find(fields, options).then((contacts) => {
                    const res = contacts.filter(c => c.phoneNumbers.some(p => p.value.replace(/[^0-9]+/g, '') === phone));
                    subscriber.next(res.length > 0 ? res[0] : null);
                }, (error) => {
                    console.error('*** pickOne Device: contacts error:', error);
                    subscriber.error(error);
                });
            }
            catch (e) {
                console.error('*** TRY/CATCH:: pickOne: Device contacts error:', e.message);
                subscriber.error('The Contacts Plugin is not installed');
            }
        });
    }
    loadFormattedNativeContacts(filter) {
        const observer = new Observable(subscriber => {
            this.loadRawNativeContacts(filter)
                .pipe(withLatestFrom(this.store.Contacts$))
                .subscribe(([contacts, stored]) => {
                const formattedContacts = contacts
                    .filter(contact => {
                    contact.name = contact.name || { givenName: '', familyName: '' };
                    return contact.name.givenName
                        && stored.findIndex(s => s.phone === (contact.phoneNumbers ? contact.phoneNumbers[0].value : '')) === -1;
                })
                    .sort((a, b) => a.name.givenName.localeCompare(b.name.givenName))
                    .map((c) => new ContactModel({
                    id: null,
                    name: c.name ? c.name.givenName : '',
                    lastName: c.name ? c.name.familyName : '',
                    type: 'NOT_SPECIFIED',
                    email: c.emails ? c.emails[0].value : '',
                    phone: c.phoneNumbers ? c.phoneNumbers[0].value : '',
                }));
                subscriber.next(formattedContacts);
            }, (error) => {
                console.error('****** loadFormatted Device contacts error:', error);
                subscriber.error(error);
            });
        });
        return observer;
    }
    loadRemoteContacts() {
        return this.repository.getContacts().pipe(map((response) => {
            return response.data.map(contact => {
                const contactModel = ContactModel.fromDataResponse(contact);
                return contactModel;
            }).sort((a, b) => b.id - a.id);
        }), catchError(error => {
            throw error;
        }));
    }
    loadCountryCodes() {
        return this.repository.getCountryCodes().pipe(map((response) => {
            return (response && response.length) ? response.sort((a, b) => a.name.localeCompare(b.name)) : COUNTRY_CALLING_CODES;
        }), catchError(error => {
            throw error;
        }));
    }
    loadContactInteractions(contactId) {
        return this.repository.getContactInteractions(contactId).pipe(map((response) => {
            return response.data.map(interaction => {
                return ContactInteractionModel.fromDataResponse(interaction);
            });
        }), catchError(error => {
            throw error;
        }));
    }
    createContact(form) {
        return this.repository.createContact(form).pipe(map((response) => {
            if (response.data.id) {
                return ContactModel.fromDataResponse(response.data);
            }
            else {
                throw new Error('Unknown error');
            }
        }), catchError(error => {
            throw error;
        }));
    }
    deleteContact(contactId) {
        return this.repository.deleteContact(contactId).pipe(map((response) => {
            if (response.status === 'success') {
                return contactId;
            }
            else {
                throw { statusCode: 500, status: 'error' };
            }
        }), catchError(error => {
            throw error;
        }));
    }
    updateContact(form) {
        return this.repository.updateContact(form).pipe(map((response) => {
            return ContactModel.fromDataResponse(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
    importContacts(contactList) {
        return this.repository.importContacts(contactList).pipe(map((response) => {
            return response.data.contacts_exported.map(c => ContactModel.fromDataResponse(c));
        }), catchError(error => {
            throw error;
        }));
    }
    createInteraction(contactId, config) {
        return this.repository.createInteraction(contactId, config).pipe(map((response) => {
            return ContactInteractionModel.fromDataResponse(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
};
ContactsService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [CONTACTS_REPOSITORY,] }] },
    { type: Contacts },
    { type: ContactStore }
];
ContactsService.ɵprov = ɵɵdefineInjectable({ factory: function ContactsService_Factory() { return new ContactsService(ɵɵinject(CONTACTS_REPOSITORY), ɵɵinject(Contacts), ɵɵinject(ContactStore)); }, token: ContactsService, providedIn: "root" });
ContactsService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(CONTACTS_REPOSITORY))
], ContactsService);

const CONTACTS_SERVICE = new InjectionToken('contactsService');

let ContactsEffects = class ContactsEffects {
    constructor(actions$, store, service) {
        this.actions$ = actions$;
        this.store = store;
        this.service = service;
        this.load$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.FetchContactBegin), switchMap(() => {
            return this.service.loadRemoteContacts().pipe(map((contacList) => FetchContactSuccessAction({ contacList })), catchError(error => {
                console.error('Couldn\'t fetch contacts');
                return of(FetchContactFailAction({ errors: error }));
            }));
        })));
        this.loadCountryCodes$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.FetchCountryCodesBegin), switchMap(() => {
            return this.service.loadCountryCodes().pipe(map((codes) => FetchCountryCodesSuccessAction({ codes })), catchError(error => {
                console.error('Couldn\'t fetch country codes', error);
                return of(FetchCountryCodesFailAction({ errors: error }));
            }));
        })));
        this.loadInteractions$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.FetchInteractionsBegin), switchMap((action) => {
            return this.service.loadContactInteractions(action.contactId).pipe(map((interactions) => FetchInteractionsSuccessAction({ interactions })), catchError(error => {
                console.error('Couldn\'t fetch contact interactions');
                return of(FetchInteractionsFailAction({ errors: error }));
            }));
        })));
        this.create$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.CreateContactBegin), switchMap((action) => {
            return this.service.createContact(action.contactForm).pipe(map((contact) => CreateContactSuccessAction({ contact })), catchError(errors => {
                console.error('Couldn\'t Create contact');
                return of(CreateContactFailAction({ errors }));
            }));
        })));
        this.delete$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.DeleteContactBegin), switchMap((action) => this.service.deleteContact(action.contactId).pipe(map(contactId => DeleteContactSuccessAction({ contactId })), catchError(errors => {
            console.error('Couldn\'t Delete contact');
            return of(DeleteContactFailAction({ errors }));
        })))));
        this.update$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.UpdateContactBegin), switchMap((action) => this.service.updateContact(action.contactForm).pipe(map((updatedContact) => UpdateContactSuccessAction({ contact: updatedContact })), catchError(errors => {
            console.error('Couldn\'t Update contact');
            return of(UpdateContactFailAction({ errors }));
        })))));
        this.import$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.ImportContactBegin), switchMap((action) => {
            return this.service.importContacts(action.contactList).pipe(map(contactList => {
                return ImportContactSuccessAction({ contactList });
            }), catchError(errors => {
                return of(ImportContactFailAction({ errors }));
            }));
        })));
        this.filter$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.FilterContactsBegin), withLatestFrom(this.store.Contacts$), switchMap(([action, storedContacts]) => {
            const contactList = action.clientType === 'ALL' ? storedContacts :
                storedContacts.filter(item => item.type === action.clientType);
            return of(FilterContactsSuccessAction({ contactList }));
        }), catchError(errors => {
            console.error('**** Filter contacts Error');
            return of(FilterContactsFailAction({ errors }));
        })));
        this.createInteraction$ = createEffect(() => this.actions$.pipe(ofType(ContactsActionTypes.CreateInteractionBegin), switchMap((action) => {
            return this.service.createInteraction(action.contactId, action.config).pipe(map(interaction => {
                return CreateInteractionSuccessAction({ interaction });
            }), catchError(errors => {
                return of(CreateInteractionFailAction({ errors }));
            }));
        })));
    }
};
ContactsEffects.ctorParameters = () => [
    { type: Actions },
    { type: ContactStore },
    { type: undefined, decorators: [{ type: Inject, args: [CONTACTS_SERVICE,] }] }
];
ContactsEffects = __decorate([
    Injectable(),
    __param(2, Inject(CONTACTS_SERVICE))
], ContactsEffects);

const initialState = {
    isLoading: false,
    items: [],
    filteredItems: [],
    selectedId: null,
    interactions: {
        isLoading: false,
        items: [],
        error: null
    },
    countryCodes: {
        items: [],
        isLoading: false,
        error: null
    },
    hasBeenFetched: false,
    error: null,
    success: null
};
const ɵ0$1 = (state) => (Object.assign(Object.assign({}, state), { isLoading: true, error: null, success: null })), ɵ1$1 = (state) => (Object.assign(Object.assign({}, state), { countryCodes: Object.assign(Object.assign({}, state.countryCodes), { error: null, isLoading: true }) })), ɵ2$1 = (state) => (Object.assign(Object.assign({}, state), { interactions: Object.assign(Object.assign({}, state.interactions), { isLoading: true, items: [], error: null }) })), ɵ3$1 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })), ɵ4$1 = (state, action) => (Object.assign(Object.assign({}, state), { countryCodes: Object.assign(Object.assign({}, state.countryCodes), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })), ɵ5$1 = (state, action) => (Object.assign(Object.assign({}, state), { interactions: Object.assign(Object.assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })), ɵ6$1 = (state, action) => (Object.assign(Object.assign({}, state), { interactions: Object.assign(Object.assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })), ɵ7$1 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: action.contacList, hasBeenFetched: true, success: { after: getSuccessActionType(action.type) } })), ɵ8 = (state, action) => (Object.assign(Object.assign({}, state), { countryCodes: Object.assign(Object.assign({}, state.countryCodes), { items: action.codes, isLoading: false }) })), ɵ9 = (state, action) => (Object.assign(Object.assign({}, state), { interactions: Object.assign(Object.assign({}, state.interactions), { isLoading: false, items: action.interactions }) })), ɵ10 = (state, action) => (Object.assign(Object.assign({}, state), { interactions: Object.assign(Object.assign({}, state.interactions), { isLoading: false, items: [action.interaction, ...state.interactions.items] }) })), ɵ11 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [action.contact, ...state.items], success: { after: getSuccessActionType(action.type) } })), ɵ12 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [...action.contactList, ...state.items], success: { after: getSuccessActionType(action.type) } })), ɵ13 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [
        ...state.items.filter((c) => c.id !== action.contactId)
    ], success: { after: getSuccessActionType(action.type) } })), ɵ14 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [
        ...((cl) => {
            const tmp = [...cl];
            const idx = cl.findIndex((m) => m.id === action.contact.id);
            if (idx !== -1) {
                tmp.splice(idx, 1, action.contact);
            }
            return tmp;
        })(state.items),
    ], success: { after: getSuccessActionType(action.type) } })), ɵ15 = (state, action) => (Object.assign(Object.assign({}, state), { filteredItems: action.contactList, isLoading: false, error: null, success: null })), ɵ16 = (state, action) => (Object.assign(Object.assign({}, state), { selectedId: action.contactId, error: null, success: null }));
const reducer = createReducer(initialState, 
// On Begin Actions
on(FetchContactBeginAction, CreateContactBeginAction, DeleteContactBeginAction, UpdateContactBeginAction, ImportContactBeginAction, ɵ0$1), on(FetchCountryCodesBeginAction, ɵ1$1), on(FetchInteractionsBeginAction, ɵ2$1), 
// ON Fail Actions
on(FetchContactFailAction, CreateContactFailAction, DeleteContactFailAction, UpdateContactFailAction, ImportContactFailAction, ɵ3$1), on(FetchCountryCodesFailAction, ɵ4$1), on(FetchInteractionsFailAction, ɵ5$1), on(CreateInteractionFailAction, ɵ6$1), 
// ON Success Actions:
// FETCH
on(FetchContactSuccessAction, ɵ7$1), on(FetchCountryCodesSuccessAction, ɵ8), on(FetchInteractionsSuccessAction, ɵ9), on(CreateInteractionSuccessAction, ɵ10), 
// INSERT Contacts:
on(CreateContactSuccessAction, ɵ11), on(ImportContactSuccessAction, ɵ12), 
// REMOVES Contacts
on(DeleteContactSuccessAction, ɵ13), 
// UPDATES Contacts
on(UpdateContactSuccessAction, ɵ14), 
// FILTER
on(FilterContactsSuccessAction, ɵ15), 
// SELECT
on(SelectContactAction, ɵ16));
function getErrorActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case ContactsActionTypes.FetchContactFail:
        case ContactsActionTypes.FetchCountryCodesFail:
            action = 'GET';
            break;
        case ContactsActionTypes.FetchInteractionsFail:
            action = 'GET_INTERACTIONS';
            break;
        case ContactsActionTypes.CreateContactFail:
        case ContactsActionTypes.CreateInteractionFail:
            action = 'CREATE';
            break;
        case ContactsActionTypes.UpdateContactFail:
            action = 'UPDATE';
            break;
        case ContactsActionTypes.DeleteContactFail:
            action = 'DELETE';
            break;
        case ContactsActionTypes.ImportContactFail:
            action = 'IMPORT';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case ContactsActionTypes.FetchContactSuccess:
            action = 'GET';
            break;
        case ContactsActionTypes.CreateContactSuccess:
            action = 'CREATE';
            break;
        case ContactsActionTypes.UpdateContactSuccess:
            action = 'UPDATE';
            break;
        case ContactsActionTypes.DeleteContactSuccess:
            action = 'DELETE';
            break;
        case ContactsActionTypes.ImportContactSuccess:
            action = 'IMPORT';
            break;
    }
    return action;
}
function contactsReducer(state, action) {
    return reducer(state, action);
}

var ContactsCoreModule_1;
let ContactsCoreModule = ContactsCoreModule_1 = class ContactsCoreModule {
    static forRoot(config) {
        return {
            ngModule: ContactsCoreModule_1,
            providers: [
                { provide: CONTACTS_SERVICE, useClass: ContactsService },
                { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository },
                ...config.providers,
                Contacts,
                ContactStore
            ]
        };
    }
};
ContactsCoreModule = ContactsCoreModule_1 = __decorate([
    NgModule({
        declarations: [
            ContactItemComponent
        ],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('contacts', contactsReducer),
            EffectsModule.forFeature([ContactsEffects]),
            CommonModule,
            IonicModule
        ],
        exports: [
            ContactItemComponent
        ]
    })
], ContactsCoreModule);

/*
 * Public API Surface of contacts-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CONTACTS_REPOSITORY, CONTACTS_SERVICE, COUNTRY_CALLING_CODES, ContactInteractionModel, ContactModel, ContactStore, ContactsActionTypes, ContactsCoreModule, ContactsEffects, ContactsRepository, ContactsService, CreateContactBeginAction, CreateContactFailAction, CreateContactSuccessAction, CreateInteractionBeginAction, CreateInteractionFailAction, CreateInteractionSuccessAction, DeleteContactBeginAction, DeleteContactFailAction, DeleteContactSuccessAction, FetchContactBeginAction, FetchContactFailAction, FetchContactSuccessAction, FetchCountryCodesBeginAction, FetchCountryCodesFailAction, FetchCountryCodesSuccessAction, FetchInteractionsBeginAction, FetchInteractionsFailAction, FetchInteractionsSuccessAction, FilterContactsBeginAction, FilterContactsFailAction, FilterContactsSuccessAction, ImportContactBeginAction, ImportContactFailAction, ImportContactSuccessAction, SelectContactAction, UpdateContactBeginAction, UpdateContactFailAction, UpdateContactSuccessAction, contactsReducer, getContactById, getContactInteractionError, getContactInteractionItems, getContactItems, getContactPageState, getContactState, getCountryCodes, getCountryCodesErrors, getError, getFilteredContactItems, getIsLoading, getIsLoadingCountryCodes, getIsLoadingInteractions, getSuccess, hasBeenFetched, initialState, stateGetFilteredItems, stateGetInteractionItems, stateGetIsLoading, stateGetIsLoadingInteractions, stateGetItems, stateHasBeenFetched, ɵ0, ɵ1, ɵ2, ɵ3, ɵ4, ɵ5, ɵ6, ɵ7, ContactItemComponent as ɵa };
//# sourceMappingURL=boxx-contacts-core.js.map
