import { __decorate, __param, __read, __assign, __spread } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, Inject, Injectable, InjectionToken, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { ContactFindOptions, Contacts } from '@ionic-native/contacts';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createFeatureSelector, createSelector, Store, createReducer, on, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { Observable, of } from 'rxjs';
import { withLatestFrom, map, catchError, switchMap } from 'rxjs/operators';

var ContactItemComponent = /** @class */ (function () {
    function ContactItemComponent() {
        this.showDetail = true;
        this.showEmail = false;
    }
    ContactItemComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], ContactItemComponent.prototype, "showDetail", void 0);
    __decorate([
        Input()
    ], ContactItemComponent.prototype, "contact", void 0);
    __decorate([
        Input()
    ], ContactItemComponent.prototype, "showEmail", void 0);
    ContactItemComponent = __decorate([
        Component({
            selector: 'boxx-contact-item',
            template: "<ion-item detail=\"{{showDetail}}\">\n    <ion-avatar slot=\"start\" class=\"boxx-contact-item-avatar\">\n        <div>{{contact.name[0]}}{{contact.lastName[0]}}</div>\n    </ion-avatar>\n    <ion-label>\n        <h2>{{contact.name}} {{contact.lastName}}</h2>\n        <p class=\"boxx-contact-item-detail email\" *ngIf=\"contact.email && showEmail\">\n            <ion-icon class=\"boxx-contact-item-detail-icon email\" name=\"mail-open\"></ion-icon>\n            {{contact.email}}\n        </p>\n        <p class=\"boxx-contact-item-detail phone\" *ngIf=\"contact.phone\">\n            <ion-icon class=\"boxx-contact-item-detail-icon phone\" name=\"call\"></ion-icon>\n            {{contact.phone}}\n        </p>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
            styles: [".contact-icon-detail{display:flex;align-items:center}.contact-icon-detail .boxx-contact-item-detail-icon{margin-right:8px}"]
        })
    ], ContactItemComponent);
    return ContactItemComponent;
}());

var ContactsRepository = /** @class */ (function () {
    function ContactsRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    ContactsRepository.prototype.getContacts = function () {
        return this.httpClient.get("" + this.getBaseUrl());
    };
    ContactsRepository.prototype.getCountryCodes = function () {
        return this.httpClient.get(
        // `https://restcountries.eu/rest/v2/all?fields=flag;alpha3Code;callingCodes;translations;name`
        './../assets/countryCodes.json');
    };
    ContactsRepository.prototype.getContactInteractions = function (contactId) {
        return this.httpClient.get(this.getBaseUrl() + "/interactions/" + contactId);
    };
    ContactsRepository.prototype.createContact = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create", body);
    };
    ContactsRepository.prototype.deleteContact = function (contactId) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete/" + contactId);
    };
    ContactsRepository.prototype.updateContact = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update/" + payload.id, body);
    };
    ContactsRepository.prototype.importContacts = function (payload) {
        var data = { export_data: JSON.stringify({ contacts: payload }) };
        var urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach(function (key) {
            urlSearchParams.append(key, data[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/export_from_mobile", body);
    };
    ContactsRepository.prototype.createInteraction = function (contactId, config) {
        var urlSearchParams = new URLSearchParams();
        Object.keys(config).forEach(function (key) {
            urlSearchParams.append(key, config[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create_interaction/" + contactId, body);
    };
    ContactsRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/contacts";
    };
    ContactsRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    ContactsRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], ContactsRepository);
    return ContactsRepository;
}());
/*export const countryCallingCodes:{[key: string]: string} = {
    //Country codes, source: https://countrycode.org

    //Note:
    //https://faq.whatsapp.com/sv/wp/21016748/?category=5245236
    //Make sure to remove any leading 0's or special calling codes
    //All phone numbers in Argentina (country code "54") should have a "9"
    // between the country code and area code. The prefix "15" must be removed so the
    // final number will have 13 digits total: +54 9 xxx xxx xxxx
    //Phone numbers in Mexico (country code "52") need to have "1" after "+52", even if they're Nextel numbers.

    'ARG': '54 9',//Argentina
    'MEX': '52 1',//México
  };*/
// Use the nex api: https://restcountries.eu/
// Example:
// https://restcountries.eu/rest/v2/{service}?fields={field};{field};{field}
// https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
// https://restcountries.eu/rest/v2/name/mexico?fields=flag;alpha3Code;callingCodes;translations
var COUNTRY_CALLING_CODES = [
    { alpha3Code: 'ARG', callingCodes: ['54'], flag: 'https://restcountries.eu/data/arg.svg', name: 'Argentina', translations: { es: 'Argentina' } },
    {
        alpha3Code: 'MEX',
        callingCodes: ['52'],
        flag: 'https://restcountries.eu/data/mex.svg',
        name: 'México', translations: { es: 'México' }
    },
    { alpha3Code: 'USA', callingCodes: ['1'], flag: 'https://restcountries.eu/data/usa.svg', name: 'Estado Unidos', translations: { es: 'Estado Unidos' } },
];

var CONTACTS_REPOSITORY = new InjectionToken('contactsRepository');

var ContactModel = /** @class */ (function () {
    function ContactModel(data) {
        this.id = data.id;
        this.name = data.name;
        this.lastName = data.lastName || '';
        this.type = data.type || 'NOT_SPECIFIED';
        this.origin = data.origin || 'UNKNOWN';
        this.email = data.email || '';
        this.phone = (data.phone || '').replace(/[^0-9]+/g, '').slice(-10);
        this.countryCode = data.countryCode || 'MEX';
        this.phoneCode = data.phoneCode || '+52';
        this.streetAddress = data.streetAddress || '';
        this.city = data.city || '';
        this.stateIso = data.stateIso || '';
        this.createdAt = data.createdAt || '';
        this.updatedAt = data.updatedAt || '';
    }
    ContactModel.toApiModel = function (contact, excludedFields) {
        if (excludedFields === void 0) { excludedFields = []; }
        var payload = {
            id: contact.id ? contact.id.toString() : null,
            name: contact.name,
            last_name: contact.lastName || '',
            type: contact.type || 'NOT_SPECIFIED',
            email: contact.email || '',
            phone: contact.phone || '',
            country_code: contact.countryCode,
            phone_code: contact.phoneCode,
            street_address: contact.streetAddress || '',
            city: contact.city || '',
            state_iso: contact.stateIso || '',
            // Maybe unused
            client_id: null,
            full_name: null,
            origin: contact.origin,
            created_at: contact.createdAt,
            updated_at: contact.updatedAt
        };
        excludedFields.forEach(function (f) {
            if (payload.hasOwnProperty(f)) {
                delete payload[f];
            }
        });
        return payload;
    };
    ContactModel.fromDataResponse = function (data) {
        return new ContactModel({
            id: +data.id,
            name: data.name,
            lastName: data.last_name,
            type: data.type,
            origin: data.origin,
            email: data.email,
            phone: data.phone,
            countryCode: data.country_code,
            phoneCode: data.phone_code,
            streetAddress: data.street_address,
            city: data.city,
            stateIso: data.state_iso,
            createdAt: data.created_at,
            updatedAt: data.updated_at
        });
    };
    ContactModel.fromContactForm = function (form) {
        return new ContactModel({
            // Required
            id: +form.id,
            name: form.name,
            countryCode: form.country_code,
            phoneCode: form.phone_code,
            lastName: form.last_name,
            type: form.type,
            phone: form.phone,
            email: form.email,
            streetAddress: form.street_address,
            city: form.city,
            stateIso: form.state_iso
        });
    };
    ContactModel.empty = function () {
        return new ContactModel({
            id: null,
            name: '',
            countryCode: null,
            phoneCode: null
        });
    };
    return ContactModel;
}());
var ContactInteractionModel = /** @class */ (function () {
    function ContactInteractionModel(data) {
        this.id = data.id;
        this.contactId = data.contactId;
        this.entity = data.entity;
        this.entityId = data.entityId;
        this.actionType = data.actionType;
        this.createdAt = data.createdAt;
        this.displayText = data.displayText || '';
    }
    ContactInteractionModel.fromDataResponse = function (data) {
        return new ContactInteractionModel({
            id: +data.id,
            contactId: +data.contact_id,
            entity: data.entity,
            entityId: +data.entity_id,
            actionType: data.action_type,
            createdAt: data.created_at,
            displayText: data.display_text,
        });
    };
    ContactInteractionModel.empty = function () {
        return new ContactInteractionModel({
            id: null,
            contactId: null,
            entity: '',
            entityId: null,
            actionType: null,
            createdAt: null,
            displayText: null
        });
    };
    return ContactInteractionModel;
}());
/*export enum ContactInteractionTypes{
    CREATE,
    UPDATE_TYPE,
    WEBSITE_FORM,
    EVENT_INVITE,
    CREATE_NOTE,
    CONTACT_EMAIL,
    CONTACT_CALL,
    CONTACT_WHATSAPP
}*/

var ContactsActionTypes;
(function (ContactsActionTypes) {
    ContactsActionTypes["FetchContactBegin"] = "[Contacs] Fetch contact begin";
    ContactsActionTypes["FetchContactSuccess"] = "[Contacs] Fetch contact success";
    ContactsActionTypes["FetchContactFail"] = "[Contacs] Fetch contact failure";
    ContactsActionTypes["FetchInteractionsBegin"] = "[Contacs] Fetch Interactions begin";
    ContactsActionTypes["FetchInteractionsSuccess"] = "[Contacs] Fetch Interactions success";
    ContactsActionTypes["FetchInteractionsFail"] = "[Contacs] Fetch Interactions failure";
    ContactsActionTypes["CreateContactBegin"] = "[Contacs] Create begin";
    ContactsActionTypes["CreateContactSuccess"] = "[Contacs] Create success";
    ContactsActionTypes["CreateContactFail"] = "[Contacs] Create failure";
    ContactsActionTypes["DeleteContactBegin"] = "[Contacs] Delete contact begin";
    ContactsActionTypes["DeleteContactSuccess"] = "[Contacs] Delete contact success";
    ContactsActionTypes["DeleteContactFail"] = "[Contacs] Delete contact failure";
    ContactsActionTypes["UpdateContactBegin"] = "[Contacs] Update contact begin";
    ContactsActionTypes["UpdateContactSuccess"] = "[Contacs] Update contact success";
    ContactsActionTypes["UpdateContactFail"] = "[Contacs] Update contact failure";
    ContactsActionTypes["ImportContactBegin"] = "[Contacs] Import contact begin";
    ContactsActionTypes["ImportContactSuccess"] = "[Contacs] Import contact success";
    ContactsActionTypes["ImportContactFail"] = "[Contacs] Import contact failure";
    ContactsActionTypes["FilterContactsBegin"] = "[Contacs] Filter contact begin";
    ContactsActionTypes["FilterContactsSuccess"] = "[Contacs] Filter contact success";
    ContactsActionTypes["FilterContactsFail"] = "[Contacs] Filter contact failure";
    ContactsActionTypes["SelectContact"] = "[Contacs] Select contact";
    ContactsActionTypes["FetchCountryCodesBegin"] = "[Contacs] Fetch Country Codes Begin";
    ContactsActionTypes["FetchCountryCodesSuccess"] = "[Contacs] Fetch Country Codes Success";
    ContactsActionTypes["FetchCountryCodesFail"] = "[Contacs] Fetch Country Codes Fail";
    ContactsActionTypes["CreateInteractionBegin"] = "[Contacs] Create Interacion Begin";
    ContactsActionTypes["CreateInteractionSuccess"] = "[Contacs] Create Interacion Success";
    ContactsActionTypes["CreateInteractionFail"] = "[Contacs] Create Interacion Fail";
})(ContactsActionTypes || (ContactsActionTypes = {}));
// Fetch contacts from remote API
var FetchContactBeginAction = createAction(ContactsActionTypes.FetchContactBegin);
var FetchContactSuccessAction = createAction(ContactsActionTypes.FetchContactSuccess, props());
var FetchContactFailAction = createAction(ContactsActionTypes.FetchContactFail, props());
var FetchInteractionsBeginAction = createAction(ContactsActionTypes.FetchInteractionsBegin, props());
var FetchInteractionsSuccessAction = createAction(ContactsActionTypes.FetchInteractionsSuccess, props());
var FetchInteractionsFailAction = createAction(ContactsActionTypes.FetchInteractionsFail, props());
// Create contacts
var CreateContactBeginAction = createAction(ContactsActionTypes.CreateContactBegin, props());
var CreateContactSuccessAction = createAction(ContactsActionTypes.CreateContactSuccess, props());
var CreateContactFailAction = createAction(ContactsActionTypes.CreateContactFail, props());
// Delete contacts
var DeleteContactBeginAction = createAction(ContactsActionTypes.DeleteContactBegin, props());
var DeleteContactSuccessAction = createAction(ContactsActionTypes.DeleteContactSuccess, props());
var DeleteContactFailAction = createAction(ContactsActionTypes.DeleteContactFail, props());
// Update contacts
var UpdateContactBeginAction = createAction(ContactsActionTypes.UpdateContactBegin, props());
var UpdateContactSuccessAction = createAction(ContactsActionTypes.UpdateContactSuccess, props());
var UpdateContactFailAction = createAction(ContactsActionTypes.UpdateContactFail, props());
// Import contacts
var ImportContactBeginAction = createAction(ContactsActionTypes.ImportContactBegin, props());
var ImportContactSuccessAction = createAction(ContactsActionTypes.ImportContactSuccess, props());
var ImportContactFailAction = createAction(ContactsActionTypes.ImportContactFail, props());
// FILTERING
var FilterContactsBeginAction = createAction(ContactsActionTypes.FilterContactsBegin, props());
var FilterContactsSuccessAction = createAction(ContactsActionTypes.FilterContactsSuccess, props());
var FilterContactsFailAction = createAction(ContactsActionTypes.FilterContactsFail, props());
var SelectContactAction = createAction(ContactsActionTypes.SelectContact, props());
var FetchCountryCodesBeginAction = createAction(ContactsActionTypes.FetchCountryCodesBegin);
var FetchCountryCodesSuccessAction = createAction(ContactsActionTypes.FetchCountryCodesSuccess, props());
var FetchCountryCodesFailAction = createAction(ContactsActionTypes.FetchCountryCodesFail, props());
var CreateInteractionBeginAction = createAction(ContactsActionTypes.CreateInteractionBegin, props());
var CreateInteractionSuccessAction = createAction(ContactsActionTypes.CreateInteractionSuccess, props());
var CreateInteractionFailAction = createAction(ContactsActionTypes.CreateInteractionFail, props());

var getContactState = createFeatureSelector('contacts');
var ɵ0 = function (state) { return state; };
var getContactPageState = createSelector(getContactState, ɵ0);
var stateGetIsLoading = function (state) { return state.isLoading; };
var stateGetItems = function (state) { return state.items; };
var stateGetIsLoadingInteractions = function (state) { return state.interactions.isLoading; };
var stateGetInteractionItems = function (state) { return state.interactions.items; };
var stateGetFilteredItems = function (state) { return state.filteredItems; };
var stateHasBeenFetched = function (state) { return state.hasBeenFetched; };
var getIsLoading = createSelector(getContactPageState, stateGetIsLoading);
var ɵ1 = function (state) { return state.error; };
var getError = createSelector(getContactPageState, ɵ1);
var ɵ2 = function (state) { return state.success; };
var getSuccess = createSelector(getContactPageState, ɵ2);
var getContactItems = createSelector(getContactPageState, stateGetItems);
var getIsLoadingInteractions = createSelector(getContactPageState, stateGetIsLoadingInteractions);
var getContactInteractionItems = createSelector(getContactPageState, stateGetInteractionItems);
var ɵ3 = function (state) { return state.interactions.error; };
var getContactInteractionError = createSelector(getContactPageState, ɵ3);
var getFilteredContactItems = createSelector(getContactPageState, stateGetFilteredItems);
var ɵ4 = function (state) { return state.countryCodes.isLoading; };
var getIsLoadingCountryCodes = createSelector(getContactPageState, ɵ4);
var ɵ5 = function (state) { return state.countryCodes.items; };
var getCountryCodes = createSelector(getContactPageState, ɵ5);
var ɵ6 = function (state) { return state.countryCodes.error; };
var getCountryCodesErrors = createSelector(getContactPageState, ɵ6);
var ɵ7 = function (state) { return state.items.filter(function (c) { return +c.id === state.selectedId; })[0]; };
var getContactById = createSelector(getContactPageState, ɵ7);
var hasBeenFetched = createSelector(getContactPageState, stateHasBeenFetched);

var ContactStore = /** @class */ (function () {
    function ContactStore(store) {
        this.store = store;
    }
    Object.defineProperty(ContactStore.prototype, "Contacts$", {
        get: function () {
            return this.store.select(getContactItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Error$", {
        get: function () {
            return this.store.select(getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Success$", {
        get: function () {
            return this.store.select(getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "Interactions$", {
        get: function () {
            return this.store.select(getContactInteractionItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "InteractionsError$", {
        get: function () {
            return this.store.select(getContactInteractionError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "InteractionsSuccess$", {
        get: function () {
            return this.store.select(getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.ContactById$ = function (id) {
        this.store.dispatch(SelectContactAction({ contactId: id }));
        return this.store.select(getContactById);
    };
    Object.defineProperty(ContactStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "LoadingInteractions$", {
        get: function () {
            return this.store.select(getIsLoadingInteractions);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.fetchContacts = function () {
        this.store.dispatch(FetchContactBeginAction());
    };
    ContactStore.prototype.fetchInteractions$ = function (contactId) {
        this.store.dispatch(FetchInteractionsBeginAction({ contactId: contactId }));
    };
    ContactStore.prototype.filterContacts = function (criteria) {
        this.store.dispatch(FilterContactsBeginAction({ clientType: criteria }));
    };
    Object.defineProperty(ContactStore.prototype, "FilteredContacts$", {
        get: function () {
            return this.store.select(getFilteredContactItems);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.importContacts = function (contactList) {
        this.store.dispatch(ImportContactBeginAction({ contactList: contactList }));
    };
    ContactStore.prototype.createContact = function (contactForm) {
        this.store.dispatch(CreateContactBeginAction({ contactForm: contactForm }));
    };
    ContactStore.prototype.deleteContact = function (contactId) {
        this.store.dispatch(DeleteContactBeginAction({ contactId: contactId }));
    };
    ContactStore.prototype.updateContact = function (contactForm) {
        this.store.dispatch(UpdateContactBeginAction({ contactForm: contactForm }));
    };
    Object.defineProperty(ContactStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.fetchCountryCodes = function () {
        this.store.dispatch(FetchCountryCodesBeginAction());
    };
    Object.defineProperty(ContactStore.prototype, "LoadingCodes$", {
        get: function () {
            return this.store.select(getIsLoadingCountryCodes);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "LoadinCodesErrors$", {
        get: function () {
            return this.store.select(getCountryCodesErrors);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactStore.prototype, "CountryCodes$", {
        get: function () {
            return this.store.select(getCountryCodes);
        },
        enumerable: true,
        configurable: true
    });
    ContactStore.prototype.createInteraction = function (contactId, config) {
        this.store.dispatch(CreateInteractionBeginAction({ contactId: contactId, config: config }));
    };
    ContactStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    ContactStore = __decorate([
        Injectable()
    ], ContactStore);
    return ContactStore;
}());

var ContactsService = /** @class */ (function () {
    function ContactsService(repository, nativeContacts, store) {
        this.repository = repository;
        this.nativeContacts = nativeContacts;
        this.store = store;
    }
    ContactsService.prototype.loadRawNativeContacts = function (filter) {
        var _this = this;
        var options = new ContactFindOptions();
        options.multiple = true;
        options.filter = filter;
        var fields = ['displayName', 'name', 'phoneNumbers'];
        var observer = new Observable(function (subscriber) {
            try {
                _this.nativeContacts.find(fields, options).then(function (contacts) {
                    subscriber.next(contacts);
                }, function (error) {
                    console.error('*** Device contacts error:', error);
                    subscriber.error(error);
                });
            }
            catch (e) {
                console.error('*** TRY/CATCH:: loadRawNativeContacts: Device contacts error:', e.message);
                subscriber.error('The Contacts Plugin is not installed');
            }
        });
        return observer;
    };
    ContactsService.prototype.pickOne = function (phone) {
        var _this = this;
        var options = new ContactFindOptions();
        options.multiple = true;
        var fields = ['phoneNumbers'];
        return new Observable(function (subscriber) {
            try {
                _this.nativeContacts.find(fields, options).then(function (contacts) {
                    var res = contacts.filter(function (c) { return c.phoneNumbers.some(function (p) { return p.value.replace(/[^0-9]+/g, '') === phone; }); });
                    subscriber.next(res.length > 0 ? res[0] : null);
                }, function (error) {
                    console.error('*** pickOne Device: contacts error:', error);
                    subscriber.error(error);
                });
            }
            catch (e) {
                console.error('*** TRY/CATCH:: pickOne: Device contacts error:', e.message);
                subscriber.error('The Contacts Plugin is not installed');
            }
        });
    };
    ContactsService.prototype.loadFormattedNativeContacts = function (filter) {
        var _this = this;
        var observer = new Observable(function (subscriber) {
            _this.loadRawNativeContacts(filter)
                .pipe(withLatestFrom(_this.store.Contacts$))
                .subscribe(function (_a) {
                var _b = __read(_a, 2), contacts = _b[0], stored = _b[1];
                var formattedContacts = contacts
                    .filter(function (contact) {
                    contact.name = contact.name || { givenName: '', familyName: '' };
                    return contact.name.givenName
                        && stored.findIndex(function (s) { return s.phone === (contact.phoneNumbers ? contact.phoneNumbers[0].value : ''); }) === -1;
                })
                    .sort(function (a, b) { return a.name.givenName.localeCompare(b.name.givenName); })
                    .map(function (c) { return new ContactModel({
                    id: null,
                    name: c.name ? c.name.givenName : '',
                    lastName: c.name ? c.name.familyName : '',
                    type: 'NOT_SPECIFIED',
                    email: c.emails ? c.emails[0].value : '',
                    phone: c.phoneNumbers ? c.phoneNumbers[0].value : '',
                }); });
                subscriber.next(formattedContacts);
            }, function (error) {
                console.error('****** loadFormatted Device contacts error:', error);
                subscriber.error(error);
            });
        });
        return observer;
    };
    ContactsService.prototype.loadRemoteContacts = function () {
        return this.repository.getContacts().pipe(map(function (response) {
            return response.data.map(function (contact) {
                var contactModel = ContactModel.fromDataResponse(contact);
                return contactModel;
            }).sort(function (a, b) { return b.id - a.id; });
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.loadCountryCodes = function () {
        return this.repository.getCountryCodes().pipe(map(function (response) {
            return (response && response.length) ? response.sort(function (a, b) { return a.name.localeCompare(b.name); }) : COUNTRY_CALLING_CODES;
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.loadContactInteractions = function (contactId) {
        return this.repository.getContactInteractions(contactId).pipe(map(function (response) {
            return response.data.map(function (interaction) {
                return ContactInteractionModel.fromDataResponse(interaction);
            });
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.createContact = function (form) {
        return this.repository.createContact(form).pipe(map(function (response) {
            if (response.data.id) {
                return ContactModel.fromDataResponse(response.data);
            }
            else {
                throw new Error('Unknown error');
            }
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.deleteContact = function (contactId) {
        return this.repository.deleteContact(contactId).pipe(map(function (response) {
            if (response.status === 'success') {
                return contactId;
            }
            else {
                throw { statusCode: 500, status: 'error' };
            }
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.updateContact = function (form) {
        return this.repository.updateContact(form).pipe(map(function (response) {
            return ContactModel.fromDataResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.importContacts = function (contactList) {
        return this.repository.importContacts(contactList).pipe(map(function (response) {
            return response.data.contacts_exported.map(function (c) { return ContactModel.fromDataResponse(c); });
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.prototype.createInteraction = function (contactId, config) {
        return this.repository.createInteraction(contactId, config).pipe(map(function (response) {
            return ContactInteractionModel.fromDataResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    ContactsService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [CONTACTS_REPOSITORY,] }] },
        { type: Contacts },
        { type: ContactStore }
    ]; };
    ContactsService.ɵprov = ɵɵdefineInjectable({ factory: function ContactsService_Factory() { return new ContactsService(ɵɵinject(CONTACTS_REPOSITORY), ɵɵinject(Contacts), ɵɵinject(ContactStore)); }, token: ContactsService, providedIn: "root" });
    ContactsService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(CONTACTS_REPOSITORY))
    ], ContactsService);
    return ContactsService;
}());

var CONTACTS_SERVICE = new InjectionToken('contactsService');

var ContactsEffects = /** @class */ (function () {
    function ContactsEffects(actions$, store, service) {
        var _this = this;
        this.actions$ = actions$;
        this.store = store;
        this.service = service;
        this.load$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.FetchContactBegin), switchMap(function () {
            return _this.service.loadRemoteContacts().pipe(map(function (contacList) { return FetchContactSuccessAction({ contacList: contacList }); }), catchError(function (error) {
                console.error('Couldn\'t fetch contacts');
                return of(FetchContactFailAction({ errors: error }));
            }));
        })); });
        this.loadCountryCodes$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.FetchCountryCodesBegin), switchMap(function () {
            return _this.service.loadCountryCodes().pipe(map(function (codes) { return FetchCountryCodesSuccessAction({ codes: codes }); }), catchError(function (error) {
                console.error('Couldn\'t fetch country codes', error);
                return of(FetchCountryCodesFailAction({ errors: error }));
            }));
        })); });
        this.loadInteractions$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.FetchInteractionsBegin), switchMap(function (action) {
            return _this.service.loadContactInteractions(action.contactId).pipe(map(function (interactions) { return FetchInteractionsSuccessAction({ interactions: interactions }); }), catchError(function (error) {
                console.error('Couldn\'t fetch contact interactions');
                return of(FetchInteractionsFailAction({ errors: error }));
            }));
        })); });
        this.create$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.CreateContactBegin), switchMap(function (action) {
            return _this.service.createContact(action.contactForm).pipe(map(function (contact) { return CreateContactSuccessAction({ contact: contact }); }), catchError(function (errors) {
                console.error('Couldn\'t Create contact');
                return of(CreateContactFailAction({ errors: errors }));
            }));
        })); });
        this.delete$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.DeleteContactBegin), switchMap(function (action) { return _this.service.deleteContact(action.contactId).pipe(map(function (contactId) { return DeleteContactSuccessAction({ contactId: contactId }); }), catchError(function (errors) {
            console.error('Couldn\'t Delete contact');
            return of(DeleteContactFailAction({ errors: errors }));
        })); })); });
        this.update$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.UpdateContactBegin), switchMap(function (action) { return _this.service.updateContact(action.contactForm).pipe(map(function (updatedContact) { return UpdateContactSuccessAction({ contact: updatedContact }); }), catchError(function (errors) {
            console.error('Couldn\'t Update contact');
            return of(UpdateContactFailAction({ errors: errors }));
        })); })); });
        this.import$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.ImportContactBegin), switchMap(function (action) {
            return _this.service.importContacts(action.contactList).pipe(map(function (contactList) {
                return ImportContactSuccessAction({ contactList: contactList });
            }), catchError(function (errors) {
                return of(ImportContactFailAction({ errors: errors }));
            }));
        })); });
        this.filter$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.FilterContactsBegin), withLatestFrom(_this.store.Contacts$), switchMap(function (_a) {
            var _b = __read(_a, 2), action = _b[0], storedContacts = _b[1];
            var contactList = action.clientType === 'ALL' ? storedContacts :
                storedContacts.filter(function (item) { return item.type === action.clientType; });
            return of(FilterContactsSuccessAction({ contactList: contactList }));
        }), catchError(function (errors) {
            console.error('**** Filter contacts Error');
            return of(FilterContactsFailAction({ errors: errors }));
        })); });
        this.createInteraction$ = createEffect(function () { return _this.actions$.pipe(ofType(ContactsActionTypes.CreateInteractionBegin), switchMap(function (action) {
            return _this.service.createInteraction(action.contactId, action.config).pipe(map(function (interaction) {
                return CreateInteractionSuccessAction({ interaction: interaction });
            }), catchError(function (errors) {
                return of(CreateInteractionFailAction({ errors: errors }));
            }));
        })); });
    }
    ContactsEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: ContactStore },
        { type: undefined, decorators: [{ type: Inject, args: [CONTACTS_SERVICE,] }] }
    ]; };
    ContactsEffects = __decorate([
        Injectable(),
        __param(2, Inject(CONTACTS_SERVICE))
    ], ContactsEffects);
    return ContactsEffects;
}());

var initialState = {
    isLoading: false,
    items: [],
    filteredItems: [],
    selectedId: null,
    interactions: {
        isLoading: false,
        items: [],
        error: null
    },
    countryCodes: {
        items: [],
        isLoading: false,
        error: null
    },
    hasBeenFetched: false,
    error: null,
    success: null
};
var ɵ0$1 = function (state) { return (__assign(__assign({}, state), { isLoading: true, error: null, success: null })); }, ɵ1$1 = function (state) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { error: null, isLoading: true }) })); }, ɵ2$1 = function (state) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: true, items: [], error: null }) })); }, ɵ3$1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ4$1 = function (state, action) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ5$1 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ6$1 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } }) })); }, ɵ7$1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: action.contacList, hasBeenFetched: true, success: { after: getSuccessActionType(action.type) } })); }, ɵ8 = function (state, action) { return (__assign(__assign({}, state), { countryCodes: __assign(__assign({}, state.countryCodes), { items: action.codes, isLoading: false }) })); }, ɵ9 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, items: action.interactions }) })); }, ɵ10 = function (state, action) { return (__assign(__assign({}, state), { interactions: __assign(__assign({}, state.interactions), { isLoading: false, items: __spread([action.interaction], state.interactions.items) }) })); }, ɵ11 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread([action.contact], state.items), success: { after: getSuccessActionType(action.type) } })); }, ɵ12 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(action.contactList, state.items), success: { after: getSuccessActionType(action.type) } })); }, ɵ13 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(state.items.filter(function (c) { return c.id !== action.contactId; })), success: { after: getSuccessActionType(action.type) } })); }, ɵ14 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread((function (cl) {
        var tmp = __spread(cl);
        var idx = cl.findIndex(function (m) { return m.id === action.contact.id; });
        if (idx !== -1) {
            tmp.splice(idx, 1, action.contact);
        }
        return tmp;
    })(state.items)), success: { after: getSuccessActionType(action.type) } })); }, ɵ15 = function (state, action) { return (__assign(__assign({}, state), { filteredItems: action.contactList, isLoading: false, error: null, success: null })); }, ɵ16 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.contactId, error: null, success: null })); };
var reducer = createReducer(initialState, 
// On Begin Actions
on(FetchContactBeginAction, CreateContactBeginAction, DeleteContactBeginAction, UpdateContactBeginAction, ImportContactBeginAction, ɵ0$1), on(FetchCountryCodesBeginAction, ɵ1$1), on(FetchInteractionsBeginAction, ɵ2$1), 
// ON Fail Actions
on(FetchContactFailAction, CreateContactFailAction, DeleteContactFailAction, UpdateContactFailAction, ImportContactFailAction, ɵ3$1), on(FetchCountryCodesFailAction, ɵ4$1), on(FetchInteractionsFailAction, ɵ5$1), on(CreateInteractionFailAction, ɵ6$1), 
// ON Success Actions:
// FETCH
on(FetchContactSuccessAction, ɵ7$1), on(FetchCountryCodesSuccessAction, ɵ8), on(FetchInteractionsSuccessAction, ɵ9), on(CreateInteractionSuccessAction, ɵ10), 
// INSERT Contacts:
on(CreateContactSuccessAction, ɵ11), on(ImportContactSuccessAction, ɵ12), 
// REMOVES Contacts
on(DeleteContactSuccessAction, ɵ13), 
// UPDATES Contacts
on(UpdateContactSuccessAction, ɵ14), 
// FILTER
on(FilterContactsSuccessAction, ɵ15), 
// SELECT
on(SelectContactAction, ɵ16));
function getErrorActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case ContactsActionTypes.FetchContactFail:
        case ContactsActionTypes.FetchCountryCodesFail:
            action = 'GET';
            break;
        case ContactsActionTypes.FetchInteractionsFail:
            action = 'GET_INTERACTIONS';
            break;
        case ContactsActionTypes.CreateContactFail:
        case ContactsActionTypes.CreateInteractionFail:
            action = 'CREATE';
            break;
        case ContactsActionTypes.UpdateContactFail:
            action = 'UPDATE';
            break;
        case ContactsActionTypes.DeleteContactFail:
            action = 'DELETE';
            break;
        case ContactsActionTypes.ImportContactFail:
            action = 'IMPORT';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case ContactsActionTypes.FetchContactSuccess:
            action = 'GET';
            break;
        case ContactsActionTypes.CreateContactSuccess:
            action = 'CREATE';
            break;
        case ContactsActionTypes.UpdateContactSuccess:
            action = 'UPDATE';
            break;
        case ContactsActionTypes.DeleteContactSuccess:
            action = 'DELETE';
            break;
        case ContactsActionTypes.ImportContactSuccess:
            action = 'IMPORT';
            break;
    }
    return action;
}
function contactsReducer(state, action) {
    return reducer(state, action);
}

var ContactsCoreModule = /** @class */ (function () {
    function ContactsCoreModule() {
    }
    ContactsCoreModule_1 = ContactsCoreModule;
    ContactsCoreModule.forRoot = function (config) {
        return {
            ngModule: ContactsCoreModule_1,
            providers: __spread([
                { provide: CONTACTS_SERVICE, useClass: ContactsService },
                { provide: CONTACTS_REPOSITORY, useClass: ContactsRepository }
            ], config.providers, [
                Contacts,
                ContactStore
            ])
        };
    };
    var ContactsCoreModule_1;
    ContactsCoreModule = ContactsCoreModule_1 = __decorate([
        NgModule({
            declarations: [
                ContactItemComponent
            ],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('contacts', contactsReducer),
                EffectsModule.forFeature([ContactsEffects]),
                CommonModule,
                IonicModule
            ],
            exports: [
                ContactItemComponent
            ]
        })
    ], ContactsCoreModule);
    return ContactsCoreModule;
}());

/*
 * Public API Surface of contacts-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CONTACTS_REPOSITORY, CONTACTS_SERVICE, COUNTRY_CALLING_CODES, ContactInteractionModel, ContactModel, ContactStore, ContactsActionTypes, ContactsCoreModule, ContactsEffects, ContactsRepository, ContactsService, CreateContactBeginAction, CreateContactFailAction, CreateContactSuccessAction, CreateInteractionBeginAction, CreateInteractionFailAction, CreateInteractionSuccessAction, DeleteContactBeginAction, DeleteContactFailAction, DeleteContactSuccessAction, FetchContactBeginAction, FetchContactFailAction, FetchContactSuccessAction, FetchCountryCodesBeginAction, FetchCountryCodesFailAction, FetchCountryCodesSuccessAction, FetchInteractionsBeginAction, FetchInteractionsFailAction, FetchInteractionsSuccessAction, FilterContactsBeginAction, FilterContactsFailAction, FilterContactsSuccessAction, ImportContactBeginAction, ImportContactFailAction, ImportContactSuccessAction, SelectContactAction, UpdateContactBeginAction, UpdateContactFailAction, UpdateContactSuccessAction, contactsReducer, getContactById, getContactInteractionError, getContactInteractionItems, getContactItems, getContactPageState, getContactState, getCountryCodes, getCountryCodesErrors, getError, getFilteredContactItems, getIsLoading, getIsLoadingCountryCodes, getIsLoadingInteractions, getSuccess, hasBeenFetched, initialState, stateGetFilteredItems, stateGetInteractionItems, stateGetIsLoading, stateGetIsLoadingInteractions, stateGetItems, stateHasBeenFetched, ɵ0, ɵ1, ɵ2, ɵ3, ɵ4, ɵ5, ɵ6, ɵ7, ContactItemComponent as ɵa };
//# sourceMappingURL=boxx-contacts-core.js.map
