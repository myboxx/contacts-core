import { OnInit } from '@angular/core';
import { ContactModel } from '../../models/contact.model';
export declare class ContactItemComponent implements OnInit {
    showDetail: boolean;
    contact: ContactModel;
    showEmail: boolean;
    constructor();
    ngOnInit(): void;
}
