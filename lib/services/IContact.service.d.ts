import { InjectionToken } from '@angular/core';
import { Contact } from '@ionic-native/contacts';
import { Observable } from 'rxjs';
import { ContactInteractionModel, ContactModel } from '../models/contact.model';
import { IContactApiProps, IContactForm, ICountryCodes, IGenericInteractionProps } from '../repositories/IContact.repository';
export interface IContactsService<T1, T2> {
    loadRemoteContacts(): Observable<T2[]>;
    loadCountryCodes(): Observable<ICountryCodes[]>;
    loadContactInteractions(contactId: number): Observable<ContactInteractionModel[]>;
    loadRawNativeContacts(filter?: string): Observable<T1[]>;
    loadFormattedNativeContacts(filter?: string): Observable<T2[]>;
    createContact(form: IContactForm): Observable<T2>;
    deleteContact(contactId: number): Observable<number>;
    updateContact(form: IContactForm): Observable<T2>;
    importContacts(contactList: IContactApiProps[]): Observable<T2[]>;
    pickOne(phone: string): Observable<T1 | null>;
    createInteraction(contactId: number, config: IGenericInteractionProps): Observable<ContactInteractionModel>;
}
export declare const CONTACTS_SERVICE: InjectionToken<IContactsService<Contact, ContactModel>>;
