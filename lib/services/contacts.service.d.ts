import { Contact, Contacts } from '@ionic-native/contacts';
import { Observable } from 'rxjs';
import { ContactInteractionModel, ContactModel } from '../models/contact.model';
import { IContactApiProps, IContactForm, IContactRepository, ICountryCodes, IGenericInteractionProps } from '../repositories/IContact.repository';
import { ContactStore } from '../state/contact.store';
import { IContactsService } from './IContact.service';
export declare class ContactsService implements IContactsService<Contact, ContactModel> {
    private repository;
    private nativeContacts;
    private store;
    constructor(repository: IContactRepository, nativeContacts: Contacts, store: ContactStore);
    loadRawNativeContacts(filter?: string): Observable<Contact[]>;
    pickOne(phone: string): Observable<Contact>;
    loadFormattedNativeContacts(filter?: string): Observable<ContactModel[]>;
    loadRemoteContacts(): Observable<ContactModel[]>;
    loadCountryCodes(): Observable<ICountryCodes[]>;
    loadContactInteractions(contactId: number): Observable<ContactInteractionModel[]>;
    createContact(form: IContactForm): Observable<ContactModel>;
    deleteContact(contactId: number): Observable<number>;
    updateContact(form: IContactForm): Observable<ContactModel>;
    importContacts(contactList: IContactApiProps[]): Observable<ContactModel[]>;
    createInteraction(contactId: number, config: IGenericInteractionProps): Observable<ContactInteractionModel>;
}
