import { Contact } from '@ionic-native/contacts';
import { Actions } from '@ngrx/effects';
import { ContactInteractionModel, ContactModel } from '../models/contact.model';
import { ICountryCodes } from '../repositories/IContact.repository';
import { IContactsService } from '../services/IContact.service';
import * as fromActions from './contact.actions';
import * as fromReducer from './contact.reducer';
import { ContactStore } from './contact.store';
export declare class ContactsEffects {
    private actions$;
    private store;
    private service;
    load$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchContactFail>) | ({
        contacList: ContactModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchContactSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    loadCountryCodes$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchCountryCodesFail>) | ({
        codes: ICountryCodes[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchCountryCodesSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    loadInteractions$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchInteractionsFail>) | ({
        interactions: ContactInteractionModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FetchInteractionsSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    create$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.CreateContactFail>) | ({
        contact: ContactModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.CreateContactSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    delete$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.DeleteContactFail>) | ({
        contactId: number;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.DeleteContactSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    update$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.UpdateContactFail>) | ({
        contact: ContactModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.UpdateContactSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    import$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.ImportContactFail>) | ({
        contactList: ContactModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.ImportContactSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    filter$: import("rxjs").Observable<({
        contactList: ContactModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FilterContactsSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.FilterContactsFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    createInteraction$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.CreateInteractionFail>) | ({
        interaction: ContactInteractionModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.ContactsActionTypes.CreateInteractionSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, store: ContactStore, service: IContactsService<Contact, ContactModel>);
}
export interface AppState {
    contacts: fromReducer.ContactState;
}
