import { CONTACT_TYPE, IContactApiProps, IContactForm, ICountryCodes, IGenericInteractionProps } from '../repositories/IContact.repository';
import { ContactInteractionModel, ContactModel } from '../models/contact.model';
export declare enum ContactsActionTypes {
    FetchContactBegin = "[Contacs] Fetch contact begin",
    FetchContactSuccess = "[Contacs] Fetch contact success",
    FetchContactFail = "[Contacs] Fetch contact failure",
    FetchInteractionsBegin = "[Contacs] Fetch Interactions begin",
    FetchInteractionsSuccess = "[Contacs] Fetch Interactions success",
    FetchInteractionsFail = "[Contacs] Fetch Interactions failure",
    CreateContactBegin = "[Contacs] Create begin",
    CreateContactSuccess = "[Contacs] Create success",
    CreateContactFail = "[Contacs] Create failure",
    DeleteContactBegin = "[Contacs] Delete contact begin",
    DeleteContactSuccess = "[Contacs] Delete contact success",
    DeleteContactFail = "[Contacs] Delete contact failure",
    UpdateContactBegin = "[Contacs] Update contact begin",
    UpdateContactSuccess = "[Contacs] Update contact success",
    UpdateContactFail = "[Contacs] Update contact failure",
    ImportContactBegin = "[Contacs] Import contact begin",
    ImportContactSuccess = "[Contacs] Import contact success",
    ImportContactFail = "[Contacs] Import contact failure",
    FilterContactsBegin = "[Contacs] Filter contact begin",
    FilterContactsSuccess = "[Contacs] Filter contact success",
    FilterContactsFail = "[Contacs] Filter contact failure",
    SelectContact = "[Contacs] Select contact",
    FetchCountryCodesBegin = "[Contacs] Fetch Country Codes Begin",
    FetchCountryCodesSuccess = "[Contacs] Fetch Country Codes Success",
    FetchCountryCodesFail = "[Contacs] Fetch Country Codes Fail",
    CreateInteractionBegin = "[Contacs] Create Interacion Begin",
    CreateInteractionSuccess = "[Contacs] Create Interacion Success",
    CreateInteractionFail = "[Contacs] Create Interacion Fail"
}
export declare const FetchContactBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchContactBegin, () => import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchContactBegin>>;
export declare const FetchContactSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchContactSuccess, (props: {
    contacList: ContactModel[];
}) => {
    contacList: ContactModel[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchContactSuccess>>;
export declare const FetchContactFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchContactFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchContactFail>>;
export declare const FetchInteractionsBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchInteractionsBegin, (props: {
    contactId: number;
}) => {
    contactId: number;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchInteractionsBegin>>;
export declare const FetchInteractionsSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchInteractionsSuccess, (props: {
    interactions: ContactInteractionModel[];
}) => {
    interactions: ContactInteractionModel[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchInteractionsSuccess>>;
export declare const FetchInteractionsFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchInteractionsFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchInteractionsFail>>;
export declare const CreateContactBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateContactBegin, (props: {
    contactForm: IContactForm;
}) => {
    contactForm: IContactForm;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateContactBegin>>;
export declare const CreateContactSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateContactSuccess, (props: {
    contact: ContactModel;
}) => {
    contact: ContactModel;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateContactSuccess>>;
export declare const CreateContactFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateContactFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateContactFail>>;
export declare const DeleteContactBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.DeleteContactBegin, (props: {
    contactId: number;
}) => {
    contactId: number;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.DeleteContactBegin>>;
export declare const DeleteContactSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.DeleteContactSuccess, (props: {
    contactId: number;
}) => {
    contactId: number;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.DeleteContactSuccess>>;
export declare const DeleteContactFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.DeleteContactFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.DeleteContactFail>>;
export declare const UpdateContactBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.UpdateContactBegin, (props: {
    contactForm: IContactForm;
}) => {
    contactForm: IContactForm;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.UpdateContactBegin>>;
export declare const UpdateContactSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.UpdateContactSuccess, (props: {
    contact: ContactModel;
}) => {
    contact: ContactModel;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.UpdateContactSuccess>>;
export declare const UpdateContactFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.UpdateContactFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.UpdateContactFail>>;
export declare const ImportContactBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.ImportContactBegin, (props: {
    contactList: IContactApiProps[];
}) => {
    contactList: IContactApiProps[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.ImportContactBegin>>;
export declare const ImportContactSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.ImportContactSuccess, (props: {
    contactList: ContactModel[];
}) => {
    contactList: ContactModel[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.ImportContactSuccess>>;
export declare const ImportContactFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.ImportContactFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.ImportContactFail>>;
export declare const FilterContactsBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FilterContactsBegin, (props: {
    clientType: CONTACT_TYPE;
}) => {
    clientType: CONTACT_TYPE;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FilterContactsBegin>>;
export declare const FilterContactsSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FilterContactsSuccess, (props: {
    contactList: ContactModel[];
}) => {
    contactList: ContactModel[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FilterContactsSuccess>>;
export declare const FilterContactsFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FilterContactsFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FilterContactsFail>>;
export declare const SelectContactAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.SelectContact, (props: {
    contactId: number;
}) => {
    contactId: number;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.SelectContact>>;
export declare const FetchCountryCodesBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchCountryCodesBegin, () => import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchCountryCodesBegin>>;
export declare const FetchCountryCodesSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchCountryCodesSuccess, (props: {
    codes: ICountryCodes[];
}) => {
    codes: ICountryCodes[];
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchCountryCodesSuccess>>;
export declare const FetchCountryCodesFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.FetchCountryCodesFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.FetchCountryCodesFail>>;
export declare const CreateInteractionBeginAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateInteractionBegin, (props: {
    contactId: number;
    config: IGenericInteractionProps;
}) => {
    contactId: number;
    config: IGenericInteractionProps;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateInteractionBegin>>;
export declare const CreateInteractionSuccessAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateInteractionSuccess, (props: {
    interaction: ContactInteractionModel;
}) => {
    interaction: ContactInteractionModel;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateInteractionSuccess>>;
export declare const CreateInteractionFailAction: import("@ngrx/store").ActionCreator<ContactsActionTypes.CreateInteractionFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ContactsActionTypes.CreateInteractionFail>>;
