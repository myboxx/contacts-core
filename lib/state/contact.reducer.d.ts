import { Action } from '@ngrx/store';
import { IContactsStateError, IContactsStateSuccess } from '../core/IStateErrorSuccess';
import { ContactInteractionModel, ContactModel } from '../models/contact.model';
import { ICountryCodes } from '../repositories/IContact.repository';
export interface ContactState {
    isLoading: boolean;
    items: ContactModel[];
    filteredItems: ContactModel[];
    selectedId: number;
    interactions: {
        isLoading: boolean;
        items: ContactInteractionModel[];
        error: any;
    };
    hasBeenFetched: boolean;
    countryCodes: {
        items: Array<ICountryCodes>;
        isLoading: boolean;
        error: any;
    };
    error: IContactsStateError;
    success: IContactsStateSuccess;
}
export declare const initialState: ContactState;
export declare function contactsReducer(state: ContactState | undefined, action: Action): ContactState;
