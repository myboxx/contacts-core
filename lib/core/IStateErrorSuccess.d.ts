import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export interface IContactsStateError extends IStateErrorBase {
    after: 'GET' | 'GET_INTERACTIONS' | 'CREATE' | 'UPDATE' | 'DELETE' | 'IMPORT' | 'UNKNOWN';
}
export interface IContactsStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'CREATE' | 'UPDATE' | 'DELETE' | 'IMPORT' | 'UNKNOWN';
}
