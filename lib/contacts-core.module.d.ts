import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class ContactsCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<ContactsCoreModule>;
}
export {};
