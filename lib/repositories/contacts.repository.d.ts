import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IContactApiProps, IContactForm, IContactInteractionsApiProps, IContactRepository, ICountryCodes, IGenericInteractionProps, IImportContactsResponse } from './IContact.repository';
import { IHttpBasicResponse, AbstractAppConfigService } from '@boxx/core';
export declare class ContactsRepository implements IContactRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    getContacts(): Observable<IHttpBasicResponse<Array<IContactApiProps>>>;
    getCountryCodes(): Observable<Array<ICountryCodes>>;
    getContactInteractions(contactId: number): Observable<IHttpBasicResponse<Array<IContactInteractionsApiProps>>>;
    createContact(payload: IContactForm): Observable<IHttpBasicResponse<IContactApiProps>>;
    deleteContact(contactId: number): Observable<IHttpBasicResponse<null>>;
    updateContact(payload: IContactForm): Observable<IHttpBasicResponse<IContactApiProps>>;
    importContacts(payload: IContactApiProps[]): Observable<IHttpBasicResponse<IImportContactsResponse>>;
    createInteraction(contactId: number, config: IGenericInteractionProps): Observable<IHttpBasicResponse<IContactInteractionsApiProps>>;
    private getBaseUrl;
}
export declare const COUNTRY_CALLING_CODES: {
    alpha3Code: string;
    callingCodes: string[];
    flag: string;
    name: string;
    translations: {
        es: string;
    };
}[];
