import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { IHttpBasicResponse } from '@boxx/core';
export interface IContactRepository {
    getContacts(): Observable<IHttpBasicResponse<IContactApiProps[]>>;
    getCountryCodes(): Observable<ICountryCodes[]>;
    getContactInteractions(contactId: number): Observable<IHttpBasicResponse<any[]>>;
    createContact(form: IContactForm): Observable<IHttpBasicResponse<IContactApiProps>>;
    deleteContact(contactId: number): Observable<IHttpBasicResponse<string>>;
    updateContact(form: IContactForm): Observable<IHttpBasicResponse<IContactApiProps>>;
    importContacts(payload: IContactApiProps[]): Observable<IHttpBasicResponse<IImportContactsResponse>>;
    createInteraction(contactId: number, config: IGenericInteractionProps): Observable<IHttpBasicResponse<IContactInteractionsApiProps>>;
}
export declare const CONTACTS_REPOSITORY: InjectionToken<IContactRepository>;
export declare type CONTACT_TYPE = 'ALL' | 'NOT_SPECIFIED' | 'PROSPECT' | 'CLIENT';
export declare type CONTACT_ORIGIN = 'MANUAL' | 'MOBILE_APP' | 'WEB' | 'WEB_APP' | 'UNKNOWN';
export declare type INTERACTION_TYPE = 'CREATE' | 'UPDATE_TYPE' | 'CONTACT_CALL' | 'CONTACT_SCHEDULED' | 'CONTACT_EMAIL';
export interface IImportContactsResponse {
    contacts_exported: Array<IContactApiProps>;
}
export interface IContactForm {
    name: string;
    last_name: string;
    type: CONTACT_TYPE;
    country_code: string;
    phone_code: string;
    id?: number;
    contact_id?: number;
    phone?: string;
    email?: string;
    street_address?: string;
    city?: string;
    state_iso?: string;
}
export interface IImportContactsForm {
    addresses: any[];
    birthday: string;
    displayName: string;
    emails: any[];
    name: {
        familyName: string;
        givenName: string;
        middleName: string;
        formatted: string;
    };
    nickname: string;
    phoneNumbers: any[];
    client_type: number;
}
export interface IContactApiProps {
    id: string;
    client_id: string;
    country_code: string;
    phone_code: string;
    name: string;
    last_name: string;
    type: CONTACT_TYPE;
    origin: CONTACT_ORIGIN;
    email: string;
    phone: string;
    street_address: string;
    state_iso: string;
    city: string;
    created_at: string;
    updated_at: string;
    full_name: string;
}
export interface IContactInteractionsApiProps {
    id: string;
    contact_id: string;
    entity: string;
    entity_id: string;
    action_type: INTERACTION_TYPE;
    created_at: string;
    display_text: string;
}
export interface IGenericInteractionProps {
    action_type: string;
    entity: string;
    entity_id: number;
}
export interface ICountryCodes {
    name: string;
    translations: {
        es?: string;
    };
    flag: string;
    alpha3Code: string;
    callingCodes: Array<string>;
}
