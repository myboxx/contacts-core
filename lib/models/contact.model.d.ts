import { CONTACT_ORIGIN, CONTACT_TYPE, IContactApiProps, IContactForm, IContactInteractionsApiProps, INTERACTION_TYPE } from '../repositories/IContact.repository';
export declare class ContactModel implements IContactModelProps {
    id: number;
    name: string;
    lastName?: string;
    type?: CONTACT_TYPE;
    origin?: CONTACT_ORIGIN;
    email?: string;
    phone?: string;
    countryCode?: string;
    phoneCode?: string;
    streetAddress?: string;
    city?: string;
    stateIso?: string;
    createdAt?: string;
    updatedAt?: string;
    constructor(data: IContactModelProps);
    static toApiModel(contact: ContactModel, excludedFields?: string[]): IContactApiProps;
    static fromDataResponse(data: IContactApiProps): ContactModel;
    static fromContactForm(form: IContactForm): ContactModel;
    static empty(): ContactModel;
}
export interface IContactModelProps {
    id: number;
    name: string;
    countryCode?: string;
    phoneCode?: string;
    lastName?: string;
    type?: CONTACT_TYPE;
    origin?: CONTACT_ORIGIN;
    email?: string;
    phone?: string;
    streetAddress?: string;
    city?: string;
    stateIso?: string;
    createdAt?: string;
    updatedAt?: string;
}
export declare class ContactInteractionModel implements IContactInteractionModelProps {
    id: number;
    contactId: number;
    entity: string;
    entityId: number;
    actionType: INTERACTION_TYPE;
    createdAt: string;
    displayText: string;
    constructor(data: IContactInteractionModelProps);
    static fromDataResponse(data: IContactInteractionsApiProps): ContactInteractionModel;
    static empty(): ContactInteractionModel;
}
export interface IContactInteractionModelProps {
    id: number;
    contactId: number;
    entity: string;
    entityId: number;
    actionType: INTERACTION_TYPE;
    createdAt: string;
    displayText: string;
}
